@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col">
        <div class="card shadow-sm border-0 shadow-hover">
            <div class="card-body">
                <h3 class="my-2 text-center text-gray-900">Registro de Servicios</h3>
                <form id="form" class="needs-validation formGeneral" novalidate>
                    <p class="my-3 text-gray-900"> Datos del servicio</p>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="no_reporte">No. Reporte</label>
                            <input type="text" name="no_reporte" class="form-control" id="no_reporte" required>

                        </div>
                    </div>
                    <div class="form-row">
                    <input type="number" hidden class="form-control" value="{{ auth()->user()->id }}" id="id_usuario" name="id_usuario"  required>
                        <div class="form-group col-md-4">
                            <label for="direccion_traslado">Direccion traslado *</label>
                            <input type="text" class="form-control" id="direccion_traslado" name="direccion_traslado" pattern="[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="traslado">Se traslado a *</label>
                            <input type="text" class="form-control" id="traslado"  pattern="[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_forma_aviso">Forma de aviso *</label>
                            <select id="id_forma_aviso"  class="as1 form-control" data-live-search="true" name="id_forma_aviso" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="encargado">Télefono</label>
                            <input type="text" class="form-control" id="telefono" pattern="[0-9]{7,8}">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_telefonista_turno">Telefonista de turno *</label>
                            <select id="id_telefonista_turno" class="ast form-control" title="Elija un telefonista."  data-live-search="true" required>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_unidad">Unidad *</label>
                            <select id="id_unidad" class="as3 form-control" data-live-search="true" name="id_unidad" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="id_categoria">Categoría *</label>
                            <select id="id_categoria"  class="as5 form-control" data-live-search="true" name="id_categoria" required onchange="selectSubCategorias(this.value);">
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_sub_categoria">Sub-Categoría Padre*</label>
                            <select id="id_sub_categoria"  class="as6 form-control" data-live-search="true" name="id_sub_categoria" required onchange="selectSubCategoriasSubCategorias(this.value)">
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_subcategoria_subcategoria">Sub-Categoría Hija*</label>
                            <select id="id_subcategoria_subcategoria"  class="as7 form-control" data-live-search="true" name="id_subcategoria_subcategoria" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="hora_salida">Hora de salida *</label>
                            <input type="time" class="form-control" id="hora_salida" required>
    
                        </div>
                        <div class="form-group col-md-4">
                            <label for="hora_entrada">Hora de entrada *</label>
                            <input type="time" class="form-control" id="hora_entrada" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_piloto">Piloto *</label>
                            <select id="id_piloto" class="as4 form-control" data-live-search="true" name="id_piloto" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="fecha">Fecha *</label>
                            <input type="date" class="form-control" id="fecha" required>
    
                        </div>
                        <div class="form-group col-md-4">
                            <label for="kilometraje_salida">Kilometros de salida *</label>
                            <input type="text" class="form-control" id="kilometraje_salida" pattern="[0-9]{1,7}"  onchange="minkilo();" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="kilometraje_entrada">Kilometros de entrada *</label>
                            <input type="number" class="form-control" id="kilometraje_entrada" min=""  required>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="id_bombero_reporte">Bombero reportador *</label>
                            <select id="id_bombero_reporte" class="asr form-control" title="Elija un bombero" data-live-search="true" required>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="observaciones">observaciones</label>
                            <input type="text" class="form-control" id="observaciones" maxlength="120">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="hora_redaccion">hora redaccion *</label>
                            <input type="time" class="form-control" id="hora_redaccion" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="codigo">codigo *</label>
                            <input type="text" class="form-control" id="codigo" required>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="asistentes">Bomberos asistentes *</label>
                            <select id="asistentes" class="asa form-control" data-live-search="true" multiple name="asistentes" >
                            </select>
                        </div>
                    </div>
                    <button id="add" class="btn btn-success">Registrar servicio</button>
                    <button id="add" type="button" onclick="RegistroPaciente()" class="btn btn-info">Agregar paciente</button>
                    <!-- <button id="edit" class="btn btn-primary">Editar</button> -->
                </form>
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="p-0">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Pacientes</h1>
                                    </div>
                                    <div class="table-responsive">
                                    <table class=" table table-bordered bg-light" id="tabla">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Dirección</th>
                                                <th>Edad</th>
                                                <th>Sexo</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodyPacientes"></tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="update producto" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title">Agregar paciente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="" id="formPacientes" class="needs-validation" novalidate>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="nombre_paciente">Nombre del paciente *</label>
                            <input type="text" class="form-control" id="nombre_paciente" name="nombre_paciente"  pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="direccion_paciente">Dirección del paciente</label>
                            <input type="text" class="form-control" id="direccion_paciente"  pattern="[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="telefono">Edad</label>
                            <input type="number" class="form-control" id="edad" min="0">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="encargado">Sexo *</label>
                            <select id="sexo" class="custom-select" name="sexo" required>
                                <option value="" selected hidden>elija el sexo</option>
                                <option value="M">M</option>
                                <option value="F">F</option>
                            </select>
                        </div>
                    </div>
                    <button id="guardar" class="btn btn-primary">Agregar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scrips')
<script src="{{ asset('js/servicios.js') }}" defer></script>
@endsection