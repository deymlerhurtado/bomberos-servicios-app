<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    use HasFactory;
    protected $fillable = [
        'direccion_traslado',
        'nombre_paciente',
        'direccion_paciente',
        'edad',
        'sexo',
        'traslado',
        'id_forma_aviso',
        'telefono',
        'id_telefonista_turno',
        'id_unidad',
        'hora_salida',
        'hora_entrada',
        'id_piloto',
        'fecha',
        'kilometraje_salida',
        'kilometraje_entrada',
        'id_bombero_reporte',
        'observaciones',
        'kilometros_recorridos',
        'hora_redaccion',
        'codigo',
        'id_subcategoria_subcategoria',
        'id_usuario',
        'no_reporte',
    ];
}
