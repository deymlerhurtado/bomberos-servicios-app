<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoriaServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategoria_servicios', function (Blueprint $table) {
            $table->id();
            $table->string('subcategoria');
            $table->text('descripcion')->nullable();
            $table->unsignedBigInteger('id_categoria');
            $table->boolean('estado');
            $table->timestamps();
            $table->foreign('id_categoria')->references('id')->on('categoria_servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategoria_servicios');
    }
}
