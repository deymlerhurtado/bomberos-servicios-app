<?php

namespace App\Http\Controllers;

use App\Models\EstadoUnidad;
use App\Models\MetodoAdquisicion;
use App\Models\Unidad;
use Illuminate\Http\Request;

class UnidadController extends Controller
{

    public function index()
    {
        return view('unidad/ListadoUnidades');
    }
    public function create(Request $request)
    {

        $validateData = $request->validate([
            'numero' => 'required',
            'placa' => 'required',
            'modelo' => 'required',
            'anio' => 'required',
            'estado_id' => 'required',
            'id_metodo_adquisicion' => 'required',
        ]);

        if ($request->hasFile('foto')) {
            $imagen = $request->file('foto');
            $imagenNombre = str_replace(' ', '', $validateData['placa']);;
            $imagenExtencion = $imagen->getClientOriginalExtension();
            $picture = date('His') . '-' . $imagenNombre . '.' . $imagenExtencion;
            $imagen->move(public_path('unidades'), $picture);
        } else {
            $picture = 'noimage.png';
        }

        $Unidad = Unidad::create([
            'numero' => $validateData['numero'],
            'placa' => $validateData['placa'],
            'modelo' => $validateData['modelo'],
            'anio' => $validateData['anio'],
            'estado_id' => $validateData['estado_id'],
            'id_metodo_adquisicion' => $validateData['id_metodo_adquisicion'],
            'foto' => $picture,

        ]);

        return $Unidad;
    }

    public function list()
    {

        $data =  Unidad::all();

        return array(
            "content" => $data,
            "totalElements" =>  10,
            "last" =>  true,
            "totalPages" =>  1,
            "first" =>  true,
            "numberOfElements" =>  10,
            "size" =>  10,
            "number" =>  10
        );
    }

    public function listService()
    {

        $data =  Unidad::join('metodo_adquisiciones', 'metodo_adquisiciones.id', 'unidades.id_metodo_adquisicion')
            ->join('estado_unidades', 'estado_unidades.id', 'unidades.estado_id')
            ->select(
                'unidades.id',
                'unidades.numero',
                'unidades.placa',
                'unidades.modelo',
                'unidades.anio',
                'unidades.foto',
                'metodo_adquisiciones.adquisicion',
                'estado_unidades.nombre as estado_unidades'
            )->get();

        return datatables($data)
            ->addColumn('btn', '<button class="btn btn-success" onclick="find({{$id}})"><i class="fas fa-edit"></i></button>
                           <button class="btn btn-danger" onclick="remove({{$id}})"><i class="fas fa-trash-alt"></i></button>')
            ->addColumn('img', '<img class="img-fluid" width="100px" height="100px" src="http://bomberoscoatepeque.com/servicios/public/unidades/{{$foto}}">')
            ->rawColumns(['btn', 'img'])
            ->make(true);
    }

    public function find($id)
    {
        return Unidad::find($id);
    }

    public function update(Request $request)
    {

        $validateData = $request->validate([
            'numero' => 'required',
            'placa' => 'required',
            'modelo' => 'required',
            'anio' => 'required',
            'estado_id' => 'required',
            'id_metodo_adquisicion' => 'required',
        ]);

        $unidad = Unidad::findOrFail($request->id);

        if (!$unidad) {
            return response()->json([
                'mensaje' => 'El registro no existe'
            ], 401);
        }

        if ($request->hasFile('foto')) {
            $imagen = $request->file('foto');
            $imagenNombre = str_replace(' ', '', $validateData['placa']);;
            $imagenExtencion = $imagen->getClientOriginalExtension();
            $picture = date('His') . '-' . $imagenNombre . '.' . $imagenExtencion;
        } else {
            $picture = $unidad->foto;
        }

        if ($picture != $unidad->foto) {
            $imagen->move(public_path('unidades'), $picture);

            $deleteImg = '/public/unidades/' . $unidad->foto;
            if ($unidad->foto != 'noimage.png') {
                if (file_exists($deleteImg)) {
                    File::delete($deleteImg);
                }
            }
        }

        $unidad->numero      = $validateData['numero'];
        $unidad->placa = $validateData['placa'];
        $unidad->modelo = $validateData['modelo'];
        $unidad->anio = $validateData['anio'];
        $unidad->estado_id = $validateData['estado_id'];
        $unidad->id_metodo_adquisicion = $validateData['id_metodo_adquisicion'];
        $unidad->foto = $picture;
        $unidad->save();

        return $unidad;
    }

    public function delete(Request $request)
    {
        try {

            $paciente = Unidad::find($request->id);
            $paciente->delete();

            return response()->json([
                'mensaje' => 'Unidad eliminada correctamente'
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'El registro está en uso'
            ], 401);
        }
    }

    public function metodoAdquisicion(Request $request)
    {
        return MetodoAdquisicion::all();
    }
    public function estadoUnidad(Request $request)
    {
        return EstadoUnidad::all();
    }

    public function unidades(Request $request)
    {
        return Unidad::all();
    }
}
