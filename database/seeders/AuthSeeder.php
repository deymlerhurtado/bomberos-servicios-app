<?php

namespace Database\Seeders;

use App\Models\Bomberos;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol = Role::create([
            'nombre' => 'Administrador',
        ]);

        $rol = Role::create([
            'nombre' => 'Registrador',
        ]);

        $Bombero = Bomberos::create([
            'id' =>1,
            'nombres' => "Administrador",
            'apellidos'  => "Administrador",
            'id_puesto' => 1,
            'dpi' => "0000000000000",
            'no_registro'=> "0001",
            'email'=> "admin@gmail.com",
            'direccion'=> "coatepeque",
            'telefono'=> "12345678",
            'fecha_nacimiento'=> "2021-11-11",
            'estado' => 1,
        ]);

        $user = User::create([
            'id' =>  1,
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('Admin123'),
            'estado' => true,
            'id_rol' => 1,
            'id_bombero' =>1,
        ]);

    }
}
