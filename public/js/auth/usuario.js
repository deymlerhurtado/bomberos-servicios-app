$(document).ready(function () {
    selectBomberos();
    selectRoles();
    if ( $.fn.dataTable.isDataTable( '#tabla' ) ) {
    }else{
        listUsers();
    }

    $("#form").on('submit', function (evt) {
        evt.preventDefault();

        if ($("#form")[0].checkValidity() == true) {

            let id = $('#id').val();
            console.log(id);
            if (id != 0 || id > 0) {
                update();
            } else {
                create();
            }
        } else {
            console.log("Form incorrecto")

        }
    });
    $("#show_hide_password").on('click', function(event) {
        event.preventDefault();
        if($('#password').attr("type") == "text"){
            $('#password').attr('type', 'password');
            $('#show_hide_password').addClass( "fa-eye-slash" );
            $('#show_hide_password').removeClass( "fa-eye" );
        }else if($('#password ').attr("type") == "password"){
            $('#password ').attr('type', 'text');
            $('#show_hide_password').removeClass( "fa-eye-slash" );
            $('#show_hide_password').addClass( "fa-eye" );
        }
    });
});
function modalCreate() {

    document.getElementsByClassName('needs-validation')[0].classList.remove("was-validated");
    $("#form input:hidden").val('');
    $('#form').trigger('reset');
    $('#edit').hide();
    $('#email').prop('readonly', false);
    $('#titulo').text('Registrar Usuario');
    $('#add').show();
    $('#modal').modal('show');
}

function selectBomberos() {
    let _url = `./api/servicios/bomberos`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.nombres} ${respuesta.apellidos}</option>`
            });
            $('#id_bombero').html(html);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectRoles() {
    let _url = `./api/roles`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.nombre}</option>`
            });
            $('#id_rol').html(html);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}


function create(){
    let name = $('#name').val();
    let email = $('#email').val();
    let password = $('#password').val();
    let estado = $('#estado').val();
    let id_rol = $('#id_rol').val();
    let id_bombero = $('#id_bombero').val();

        let _url= `./api/registerUser`;
        let _token   = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: _url,
        type: 'post',
        data : {
            name: name,
            email: email,
            password: password,
            estado: estado,
            id_rol:id_rol,
            id_bombero:id_bombero,
            _token: _token},
        dataType: 'JSON',
        success: function (response) {
            $('#tabla').DataTable().ajax.reload();
            if(response){
            console.log(response);
            $("#modal").modal('hide');
            toastr.success("Creado con exito");
            }
        },
        error: function (errormessage) {
            toastr.error ("El correo ya ha sido registrado");

            console.log(errormessage);
        }
    });

}

function listUsers() {
    $('#tabla').DataTable( {
        autoWidth: false,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "todo"]],
        "order": [[ 0, "desc" ]],
        "ajax": "./api/auth/list",
        "columns":[
            {data:'name'},
            {data:'email'},
            {data:'rol'},
            {data:'nombre'},
            {data:'estado'},
            {data:'btn'},
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'print',
                title: 'Reporte colaboradores'
               }
        ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                buttons: {
                    print: 'imprimir'
                }
                
            }
    } );

}

function find(id){
    let _url = `./api/auth/find?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            $('#form').trigger('reset');

            $('#email').prop('readonly', true);
            $('#id').val(response.id);
            $('#name').val(response.name);
            $('#email').val(response.email);
            $('#estado').val(response.estado);
            $('#id_rol').val(response.id_rol);
            $('#id_bombero').val(response.id_bombero);
            $("#titulo").text('Editar usuario');
            $('#edit').show();
            $('#add').hide();
            $('#form').attr('action', '');
            $("#modal").modal('show');
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });

}

function update(){
    let _url= `./api/auth/update`;
    let _token   = $('meta[name="csrf-token"]').attr('content');
    let name = $('#name').val();
    let estado = $('#estado').val();
    let id_rol = $('#id_rol').val();
    let id_bombero = $('#id_bombero').val();
    let password = $('#password').val();
    var id= $('#id').val();

    $.ajax({
        url: _url,
        type: 'put',
        data : {
            id: id,
            name: name,
            id_rol: id_rol,
            id_bombero:id_bombero,
            password:password,
            estado: estado,
            _token: _token},
        dataType: 'JSON',
        success: function (response) {
            $('#tabla').DataTable().ajax.reload();
            document.getElementsByClassName('needs-validation')[0].classList.remove("was-validated");
            $("#modal").modal('hide');
            toastr.success("actualizado con exito");
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });

    
    
}

function remove(id){
    Swal.fire({
        title: '¿esta seguro?',
        text: "¿seguro que quieres eliminar?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'si, seguro!'
      }).then((result) => {
        if (result.isConfirmed) {

        let _url = `./api/auth/delete?id=${id}`;
        let _token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: _url,
            type: 'delete',
            data: {
            },
            dataType: 'JSON',
            success: function (response) {
                if (response) {
                    console.log(response);
                    $('#tabla').DataTable().ajax.reload();
                    toastr.success("eliminado");

                }
            },
            error: function (errormessage) {
                console.log(errormessage.responseText);
            }
        });
      
    }
  })
}