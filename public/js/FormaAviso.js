$(document).ready(function() {
    if ( $.fn.dataTable.isDataTable( '#tabla' ) ) {
    }else{
           Listado();
        }

        $("#formUpdate").on('submit', function (evt) {
            evt.preventDefault();
    
            if ($("#formUpdate")[0].checkValidity() == true) {
    
                let id = $('#id').val();
                console.log(id);
                if (id != 0 || id > 0) {
                    update();
                } else {
                    CrearFormaAviso();
                }
            } else {
                console.log("Form incorrecto")
    
            }
        });
} );

function Listado(){
    $("#tabla").dataTable().fnDestroy()

    $('#tabla').DataTable( {
        
        autoWidth: false,
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": "./api/formaAvisos/list",
        "columns":[
            {data:'forma'},
            {data:'descripcion'},
            {data:'estado',
            render: function(data, type, row) {
  
                if(row.estado == 1) {
                    return "Activo";
                }else{
                    return "Inactivo";  
                }
                }
            },
            {data:'btn'},
        ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
    } );
}
function modalCreate() {
    $("#formUpdate input:hidden").val('');

    $('#btneditar').hide();
    $('#title').text('Crear nueva forma de aviso');
    $('#guardar').show();
    $('#modal').modal('show');
}
function CrearFormaAviso() {

    let forma = $('#forma').val();
    let descripcion = $('#descripcion').val();
    let estado = $('#estado').val();

    let _url = `./api/formaAvisos/create`;
    let _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: _url,
        type: 'post',
        data: {
            forma: forma,
            descripcion: descripcion,
            estado: estado,
            _token: _token
        },
        dataType: 'JSON',
        success: function (response) {
            if (response) {
                console.log(response);
                $('#formUpdate').trigger("reset");
                $('#tabla').DataTable().ajax.reload();
                $('#modal').modal('hide');
            }
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });

}
function find(id){
    let _url = `./api/formaAvisos/find?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {
        },
        dataType: 'JSON',
        success: function (response) {
            if (response) {
                $('#id').val(response.id);
                $('#forma').val(response.forma);
                $('#descripcion').val(response.descripcion);
                $('#estado').val(response.estado);
                $('#title').text('Editar forma de aviso');
                $('#guardar').hide();
                $('#btneditar').show();
                $('#modal').modal('show');
            }
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function update(){
    let id = $('#id').val();
    let forma = $('#forma').val();
    let descripcion = $('#descripcion').val();
    let estado = $('#estado').val();
    let _url = `./api/formaAvisos/update`;
    let _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: _url,
        type: 'put',
        data: {
            id: id,
            forma: forma,
            descripcion: descripcion,
            estado: estado,
            _token: _token
        },
        dataType: 'JSON',
        success: function (response) {
            if (response) {
                console.log(response);
                $('#formUpdate').trigger("reset");
                $("#formUpdate input:hidden").val(' ');
                $('#tabla').DataTable().ajax.reload();
                $('#modal').modal('hide');
            }
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function remove(id){
    Swal.fire({
        title: '¿esta seguro?',
        text: "¿seguro que quieres eliminar?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'si, seguro!'
      }).then((result) => {
        if (result.isConfirmed) {

            let _url = `./api/formaAvisos/delete?id=${id}`;
            let _token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: _url,
            type: 'delete',
            data: {
            },
            dataType: 'JSON',
            success: function (response) {
                if (response) {
                    console.log(response);
                    $('#tabla').DataTable().ajax.reload();
                    toastr.success("eliminado");

                }
            },
            error: function (errormessage) {
                toastr.error("El registro está en uso");

                console.log(errormessage.responseText);
            }
        });
      
    }
  })
}
