<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PuestoBomberos extends Model
{
    use HasFactory;
    protected $fillable = [
        'puesto',
        'descripcion',
        'estado'
    ];
}
