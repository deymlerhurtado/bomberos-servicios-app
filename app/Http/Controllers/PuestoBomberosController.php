<?php

namespace App\Http\Controllers;

use App\Models\PuestoBomberos;
use Illuminate\Http\Request;

class PuestoBomberosController extends Controller
{
    public function index()
    {
        return view('puestosBomberos/listPuestos');
    }
    public function create(Request $request)
    {

        $validateData = $request->validate([
            'puesto' => 'required|string|max:255',
            'estado' => 'required'
        ]);

        $puestoBombero = PuestoBomberos::create([
            'puesto' => $validateData['puesto'],
            'descripcion' => $request->descripcion,
            'estado' => $validateData['estado'],
        ]);

        return response()->json([
            'mensaje' => 'Puesto registrado exitosamente.'
        ], 200);
    }

    public function listJson()
    {

        return PuestoBomberos::all();
    }
    public function list()
    {

        $list = PuestoBomberos::all();
        return datatables($list)
            ->addColumn('btn', '<button class="btn btn-success" onclick="find({{$id}})"><i class="fas fa-edit"></i></button>
                           <button class="btn btn-danger" onclick="remove({{$id}})"><i class="fas fa-trash-alt"></i></button>')
            ->rawColumns(['btn'])
            ->make(true);
    }

    public function find(Request $request)
    {
        return PuestoBomberos::find($request->id);
    }

    public function update(Request $request)
    {

        $validateData = $request->validate([
            'id' => 'required',
            'puesto' => 'required|string|max:255',
            'estado' => 'required',
        ]);

        $puestoBombero = PuestoBomberos::findOrFail($request->id);

        if (!$puestoBombero) {
            return response()->json([
                'mensaje' => 'El registro no existe'
            ], 401);
        }

        $puestoBombero->puesto      = $validateData['puesto'];
        $puestoBombero->descripcion = $request->descripcion;
        $puestoBombero->estado      = $validateData['estado'];
        $puestoBombero->save();

        return response()->json([
            'mensaje' => 'Puesto modificado correctamente'
        ], 200);
    }

    public function puestos(Request $request)
    {
        return PuestoBomberos::all()->where('estado', true);
    }
    public function delete(Request $request)
    {

        try {
            $paciente = PuestoBomberos::find($request->id);
            $paciente->delete();

            return response()->json([
                'mensaje' => 'Categoria eliminada correctamente'
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'El registro está en uso'
            ], 401);
        }
    }
}
