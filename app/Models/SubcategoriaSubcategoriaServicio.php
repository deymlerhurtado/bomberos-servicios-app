<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubcategoriaSubcategoriaServicio extends Model
{
    use HasFactory;
    protected $fillable = [
        'subcategoria',
        'descripcion',
        'id_subcategoria',
        'estado'
    ];
}
