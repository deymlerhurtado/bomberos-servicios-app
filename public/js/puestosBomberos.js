
$(document).ready(function() {
   
    if ( $.fn.dataTable.isDataTable( '#tablaB' ) ) {
    }else{
           tabla();
        }

        
    $("#form").on('submit', function (evt) {
        evt.preventDefault();

        if ($("#form")[0].checkValidity() == true) {

            let id = $('#id').val();
            console.log(id);
            if (id != 0 || id > 0) {
                editar();
            } else {
                guardarP();
            }
        } else {
            console.log("Form incorrecto")

        }
    });
} );
function tabla(){
    $('#tablaB').DataTable( {
        
        autoWidth: false,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "todo"]],
        "ajax": "./api/puestoBomberos/list",
        "columns":[
            {data:'puesto'},
            {data:'descripcion'},
            {data:'estado',
            render: function(data, type, row) {
  
                if(row.estado == 1) {
                    return "Activo";
                }else{
                    return "Inactivo";  
                }
                }
            },
            {data:'btn'},
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'print',
                title: 'Reporte Puesto Bomberos'
               }
        ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                buttons: {
                    print: 'Imprimir'
                }
                
            }
    } );
}

function modalCreate() {
    document.getElementsByClassName('needs-validation')[0].classList.remove("was-validated");
    $('#form').trigger("reset");
    $('#edit').hide();
    $('#add').show();
    $('#title').text('Crear nuevo puesto');
    $('#modalPuestos').modal('show');
}
function guardarP(){
   
    let puesto = $('#puesto').val();
    let descripcion = $('#descripcion').val();
    let estado = $('#estado').val();
    
        let _url = `./api/puestoBomberos/create`;
        let _token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: _url,
            type: 'post',
            data: {
                puesto: puesto,
                descripcion: descripcion,
                estado: estado,
                _token: _token
            },
            dataType: 'JSON',
            success: function (response) {
                if (response) {
                    console.log(response);
                    $('#form').trigger("reset");
                    $('#modalPuestos').modal('hide');
                    $('#tablaB').DataTable().ajax.reload();
                    document.getElementsByClassName('needs-validation')[0].classList.remove("was-validated");
                }
            },
            error: function (errormessage) {
                console.log(errormessage.responseText);
            }
        });
}
function find(id){
    let _url = `./api/puestoBomberos/find?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {
        },
        dataType: 'JSON',
        success: function (response) {
            if (response) {
                $('#id').val(response.id);
                $('#puesto').val(response.puesto);
                $('#descripcion').val(response.descripcion);
                $('#estado').val(response.estado);
                $('#guardar').hide();
                $('#btneditar').show();
                $('#title').text('editar puesto');
                $('#modalPuestos').modal('show');
            }
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
    $("#modalPuestos").modal('show');
}
function editar(){
    let id = $('#id').val();
    let puesto = $('#puesto').val();
    let descripcion = $('#descripcion').val();
    let estado = $('#estado').val();
    let _url = `./api/puestoBomberos/update`;
    let _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: _url,
        type: 'put',
        data: {
            id: id,
            puesto: puesto,
            descripcion: descripcion,
            estado: estado,
            _token: _token
        },
        dataType: 'JSON',
        success: function (response) {
            if (response) {
                console.log(response);
                $('#form').trigger("reset");
                $('#tablaB').DataTable().ajax.reload();
                document.getElementsByClassName('needs-validation')[0].classList.remove("was-validated");
                $("#modalPuestos").modal('hide');
            }
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function remove(id){
    Swal.fire({
        title: '¿esta seguro?',
        text: "¿seguro que quieres eliminar?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'si, seguro!'
      }).then((result) => {
        if (result.isConfirmed) {

            let _url = `./api/puestoBomberos/delete?id=${id}`;
        let _token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: _url,
            type: 'delete',
            data: {
            },
            dataType: 'JSON',
            success: function (response) {
                if (response) {
                    console.log(response);
                    $('#tabla').DataTable().ajax.reload();
                    toastr.success("eliminado");

                }
            },
            error: function (errormessage) {
                toastr.error("El registro está en uso");

                console.log(errormessage.responseText);
            }
        });
      
    }
  })
}
