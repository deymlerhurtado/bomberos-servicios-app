@extends('layouts.app')

@section('content')
<div class="card-body p-0">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h2 class=" text-gray-900 mb-2">Servicios Realizados</h2>
                        </div>
                        <table class="table table-bordered responsive bg-light table-sm" width="100%" cellspacing="0" id="tabla">
                            <thead>
                                <tr>
                                    <th>#</th>                                    
                                    <th>No. Reporte</th>
                                    <th>Fecha</th>
                                    <th>Atendidos</th>
                                    <th>Placa Unidad</th>
                                    <th>hora salida</th>
                                    <th>kms recorridos</th>
                                    <th>Piloto</th>
                                    <th>Categoria</th>
                                    <th>Direccion traslado</th>
                                    <th>Usuario</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="titulo">Detalle del servicio</h5>
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            
            <div class="card shadow-sm border-0 shadow-hover">
                <div class="card-body">
                    <h5 class="text-center font-weight-bold">No. Reporte: <span class="badge badge-secondary" id="id">New</span></h5>
                    <p class="text-center mb-2 font-weight-bold text-gray-900">Fecha reporte: <span class="badge text-gray-700" id="fecha">New</span> Hora: <span class="badge text-gray-700" id="hora">New</span></p>
                    <div class="table-responsive">
                        <h6 class="mt-2">datos del servicio</h6>
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                                  <th scope="col">No. Reporte</th>
                                  <th scope="col">Codigo</th>
                                  <th scope="col">Direccion Traslado</th>
                                  <th scope="col">se traslado A:</th>
                                  <th scope="col">Forma de aviso</th>
                                  <th scope="col">Telefono</th>
                                  <th scope="col">telefonista</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td id="no_reporte" scope="row"></td>
                                  <td id="codigo" scope="row"></td>
                                  <td id="direccion_traslado"></td>
                                  <td id="traslado"></td>
                                  <td id="forma_abiso"></td>
                                  <td id="telefono"></td>
                                  <td id="telefonista_turno">@</td>
                                </tr>
                              </tbody>
                        </table>
                        
                    </div>
                    <h6 class="mt-2">datos de mobilizacion</h6>
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                                  <th scope="col">Unidad</th>
                                  <th scope="col">Placa</th>
                                  <th scope="col">Kms recorridos</th>
                                  <th scope="col">Kms salida</th>
                                  <th scope="col">Kms entrada</th>
                                  <th scope="col">Hora salida</th>
                                  <th scope="col">hora entrada</th>
                                  <th scope="col">piloto</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td id="unidad" scope="row"></td>
                                  <td id="placa"></td>
                                  <td id="km_recorrido"></td>                                  
                                  <td id="km_salida"></td>
                                  <td id="km_entrada"></td>
                                  <td id="hora_salida"></td>
                                  <td id="hora_entrada"></td>
                                  <td id="piloto"></td>
                                </tr>
                              </tbody>
                        </table>
                    </div>
                    <h6 class="mt-2">Bomberos asistentes:</h6>
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm" id="tablaAsistentes">
                            <thead>
                                <tr>
                                  <th scope="col">Nombre</th>
                                  <th scope="col">Apellido</th>
                                  <th scope="col">dpi</th>
                                  <th scope="col">telefono</th>
                                  <th scope="col">No registro</th>
                                  <th scope="col">direccion</th>
                                  <th scope="col">puesto</th>
                                </tr>
                              </thead>
                              <tbody id="ayudantes">
                              </tbody>
                        </table>
                  </div>
                  <h6 class="mt-2">Entidades atendidas:</h6>
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm" id="pacientesAtendidosTable">
                            <thead>
                                <tr>
                                  <th scope="col">Nombre</th>
                                  <th scope="col">edad</th>
                                  <th scope="col">sexo</th>
                                  <th scope="col">direccion</th>
                                </tr>
                              </thead>
                              <tbody id="atendidos">
                              </tbody>
                        </table>
                    </div>
                    <p class="mb-2 font-weight-bold text-gray-900">Observaciones: <p class="text-gray-700" id="observaciones">New</p></p>
                </div>
                <button class="button btn btn-success" onclick="printReport()">Imprimir</button>
            </div>
        </div>
      </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar Servicio</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
          <div class="card shadow-sm border-0 shadow-hover">
              <div class="card-body">
                <form id="form" class="needs-validation" novalidate>
                  <input type="hidden" value="" id="idu" class="form-control">
                    <p class="my-3 text-gray-900"> Datos del servicio</p>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="no_reporteU">No. Reporte</label>
                            <input type="text" name="no_reporteU" class="form-control" id="no_reporteU" required>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="direccion_traslado">Direccion traslado *</label>
                            <input type="text" class="form-control" id="direccion_trasladou" name="direccion_traslado" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="traslado">Se traslado a *</label>
                            <input type="text" class="form-control" id="trasladou" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_forma_aviso">Forma de aviso *</label>
                            <select id="id_forma_avisou" class="custom-select" name="id_forma_avisou" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="encargado">Télefono</label>
                            <input type="number" class="form-control" id="telefonou">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_telefonista_turno">Telefonista de turno *</label>
                            <select id="id_telefonista_turnou" class="custom-select" name="id_telefonista_turnou" required>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_unidad">Unidad *</label>
                            <select id="id_unidadu" class="custom-select" name="id_unidadu" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="id_categoria">Categoría *</label>
                            <select id="id_categoriau" class="custom-select" name="id_categoria" required onchange="selectSubCategorias(this.value, null);">
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_sub_categoria">Sub-Categoría *</label>
                            <select id="id_sub_categoriau" class="custom-select" name="id_sub_categoria" required onchange="selectSubCategoriasSubCategorias(this.value, null)">
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_subcategoria_subcategoriaU">Sub-Categoría Hija*</label>
                            <select id="id_subcategoria_subcategoriaU"  class="as7 form-control" data-live-search="true" name="id_subcategoria_subcategoriaU" required>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="hora_salida">Hora de salida *</label>
                            <input type="time" class="form-control" id="hora_salidau" required>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="hora_entrada">Hora de entrada *</label>
                            <input type="time" class="form-control" id="hora_entradau" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_piloto">Piloto *</label>
                            <select id="id_pilotou" class="custom-select" name="id_pilotou" required>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="fecha">Fecha *</label>
                            <input type="date" class="form-control" id="fechau" required>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="kilometraje_salida">Kilometros de salida *</label>
                            <input type="number" class="form-control" id="kilometraje_salidau" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="kilometraje_entrada">Kilometros de entrada *</label>
                            <input type="number" class="form-control" id="kilometraje_entradau" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="id_bombero_reporte">Bombero reportador *</label>
                            <select id="id_bombero_reporteu" class="custom-select" name="id_bombero_reporte" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="observaciones">observaciones</label>
                            <input type="text" class="form-control" id="observacionesu">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="hora_redaccion">hora redaccion *</label>
                            <input type="time" class="form-control" id="hora_redaccionu" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="codigo">codigo *</label>
                            <input type="text" class="form-control" id="codigou" required>

                        </div>
                    </div>
                    <button id="add" class="btn btn-success">Guardar Cambios</button>
                    <!-- <button id="edit" class="btn btn-primary">Editar</button> -->
                </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scrips')
<script src="{{ asset('js/servicios/listaservicios.js') }}" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="{{ asset('js/validator.js') }}"></script>
@endsection