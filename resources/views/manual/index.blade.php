@extends('layouts.app')
@section('content')
<style>#canvas_container{width: 100%;height: 425px;overflow: auto;background: #333;text-align: center;border: solid 3px;}</style>

<h4 style="text-align:center">Manual de usuario</h4>
    <div id="my_pdf_viewer">
        <div id="canvas_container" >
            <canvas id="pdf_renderer"></canvas>
        </div>
        <br>
        <div id="navigation_controls">
            <button id="go_previous" class="btn btn-primary">Anterior</button>
            <input type="number" id="current_page" value="1">
            <button id="go_next" class="btn btn-primary">Siguiente</button>
            
            <button id="zoom_out" class="btn btn-success" >-</button>
            <button id="zoom_in" class="btn btn-success">+</button>

            <a href="{{route('manual.download')}}" id="download" class="btn btn-dark">Descargar</a>
        </div>   
        <!-- <div id="zoom_controls">
            <button id="zoom_out" class="btn btn-success" >-</button>
            <button id="zoom_in" class="btn btn-success">+</button>
        </div> -->
    </div>
@endsection
@section('scrips')
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js">
</script>
<script src="{{ asset('js/manual.js') }}" defer></script>
@endsection