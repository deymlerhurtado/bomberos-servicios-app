
@extends('layouts.app')
@section('content')
<html>
   <head>

   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart2);
      var analytics2 = <?php echo $unidad; ?>


      function drawChart2() {
        var data = google.visualization.arrayToDataTable(analytics2);

        var options = {
          title: '',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }

     
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart3);

        var analytics3 = <?php echo $sexo; ?>

      function drawChart3() {
        var data = google.visualization.arrayToDataTable(analytics3);


      var options = {
        legend: 'none',
        pieSliceText: 'label',
        title: '',
        pieStartAngle: 100,
      };

        var chart = new google.visualization.PieChart(document.getElementById('piechart3'));
        chart.draw(data, options);
      }


      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart4);
      var analytics4 = <?php echo $lugar; ?>
      
      function drawChart4() {

        var data = google.visualization.arrayToDataTable(analytics4);

        var options = {
          title: '',
          is3D: true,
          
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart4'));

        chart.draw(data, options);
      }


     


      
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart6);
      var analytics6 = <?php echo $categoriasi; ?>
      
      function drawChart6() {

        var data = google.visualization.arrayToDataTable(analytics6);

        var options = {
          title: '',
          is3D: true,
          
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart6'));

        chart.draw(data, options);
      }


      


    </script>

   </head>
   <body>
      <!-- Page Heading -->
      <form  href="{{ route('store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
      @method('POST')
      @csrf

   <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h4 class=" text-gray-900 mb-2">Bomberos Municipales de Coatepeque</h4>
                        </div>

      <div class="d-sm-flex align-items-center justify-content-between mb-12">
        <div class="col-xl-12 col-md-12 mb-12">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                           Visualizar Información
                        </div>
                        <div class="form-group col-md-5">
                            <label for="fecha">Desde *</label>
                            <input type="date" class="form-control" id="desde" name="desde" required>
                            <label for="fecha">Hasta *</label>
                            <input type="date" class="form-control" id="hasta" name="hasta" required>
                            <br>


                            <button type="submit" class="btn btn-success btn-icon-split">
                            <span class="icon text-white-50">
                            <i class="fas fa-calendar"></i>
                            </span>
                            <span class="text">Estadística por fecha</span>       
                           </button>

                           <a href="{{ url('/') }}" class="btn btn-primary btn-icon-split">
                           <span class="icon text-white-50">
                           <i class="fas fa-database"></i>
                           </span>
                           <span class="text">Ver Informe General</span>
                           </a>

               </div>
         </div>
      </div>
      <!-- Content Row -->
      <div class="row">
         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
               <div class="card-body">
                  <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                           Servicios Realizados
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_servicios; ?></div>
                     </div>
                     <div class="col-auto">
                        <img class="img-fluid" src="{{ asset('img/servicio.png') }}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
               <div class="card-body">
                  <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                           Entidades Atendidas
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_pacientes; ?></div>
                     </div>
                     <div class="col-auto">
                        <img class="img-fluid" src="{{ asset('img/paciente.png') }}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
               <div class="card-body">
                  <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                           Elementos Registrados Activos
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $bomberos_activos; ?></div>
                     </div>
                     <div class="col-auto">
                        <img class="img-fluid" src="{{ asset('img/bombero.png') }}">
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="col-md-12 col-xl-12">
         <div class="card mb-12">
            <div class="card-img-top" id="piechart6" style="width: 100%;"></div>
            <div class="card-body">
            <h6><strong>Servicios Realizados por Categoria</strong></h6>
            </div>
         </div>
        </div>

         <div class="col-md-12 col-xl-4">
         <div class="card mb-12">
            <div class="card-img-top" id="donutchart" style="width: 100%;"></div>
            <div class="card-body">
            <h6><strong>Unidades Utilizadas</strong></h6>
            </div>
         </div>
        </div>

        <div class="col-md-12 col-xl-4">
         <div class="card mb-12">
            <div class="card-img-top" id="piechart3" style="width: 100%;"></div>
            <div class="card-body">
            <h6><strong>Sexo de los pacientes atendidos</strong></h6>
            </div>
         </div>
        </div>

        <div class="col-md-12 col-xl-4">
         <div class="card mb-12">
            <div class="card-img-top" id="piechart4" style="width: 100%;"></div>
            <div class="card-body">
            <h6><strong>Lugares de traslado</strong></h6>
            </div>
         </div>
        </div>



         

      </div>

      </div>
      </div>
      </div>
      </div>
      </div>
      </form>
   </body>
</html>
@endsection