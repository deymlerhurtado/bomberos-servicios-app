@extends('layouts.app')
@section('content')
<div class="card-body p-0">
    <div class="py-2">
        <a href="#" onclick="modalCreate();" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
            </span>
            <span class="text">Nuevo Colaborador</span>
        </a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h2 class=" text-gray-900 mb-2">Colaboradores</h2>
                        </div>
                        <table class="table table-bordered responsive bg-light table-sm" width="100%" cellspacing="0" id="tabla">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>dpi</th>
                                    <th>Puesto</th>
                                    <th>correo</th>
                                    <th>edad</th>
                                    <th>telefono</th>
                                    <th>No. Registro</th>
                                    <th>Direccion</th>
                                    <th>estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title my-2" id="titulo">editr Colaboradores</h5>
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="card shadow-sm border-0 shadow-hover">
                <div class="card-body">
                <p class="my-2 text-center">los campos marcados con * son obligatorios</p>
                <form id="form" class="needs-validation" novalidate>
                        <input type="hidden" value="" id="id" class="form-control">
                        <div class="form-row">                            
                            <div class="form-group col-md-6">
                                <label for="nombres">Nombre *</label>
                                <input type="text" class="form-control" id="nombres" name="nombre" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="apellidos">Apellido *</label>
                                <input type="text" class="form-control" id="apellidos" name="apellido" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" required>
                            </div>
                        </div>
                        <div class="form-row">
                        <div class="form-group col-md-6">
                                <label for="dpi">dpi / cui *</label>
                                <input type="text" class="form-control" id="dpi" pattern="[0-9]{7,16}" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="telefono">telefono</label>
                                <input type="text" class="form-control" id="telefono" pattern="[0-9]{8}" required>
                            </div>
                        </div>
                        <div class="form-row">                            
                            <div class="form-group col-md-6">
                                <label for="fecha_nacimiento">fecha nacimiento *</label>
                                <input type="date" class="form-control" id="fecha_nacimiento" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="encargado">estado *</label>
                                <select id="estado"  class="custom-select" name="estado" required>
                                    <option value="" selected hidden>elija un estado</option>
                                    <option value="1">activo</option>
                                    <option value="0">inactivo</option>
                                </select>
                            </div>
                        </div>  
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="no_registro">No. registro *</label>
                                <input type="number" class="form-control" id="no_registro" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="encargado">Puesto *</label>
                                    <select id="id_puesto"  class="custom-select" name="id_puesto" required>
                                    </select>
                            </div>                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Direccion">Direccion *</label>
                                <input type="text" class="form-control" id="direccion" name="direccion" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="Direccion">Correo</label>
                                <input type="email" class="form-control" id="email" name="correo" required>
                            </div>
                        </div>
                        <button id="add"  class="btn btn-primary">Agregar</button>
                        <button id="edit" class="btn btn-primary">Editar</button>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('scrips')
    <script src="{{ asset('js/bomberos/bomberos.js') }}" defer></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="{{ asset('js/validator.js') }}"></script>
@endsection
