<?php

namespace Database\Seeders;

use App\Models\EstadoUnidad;
use Illuminate\Database\Seeder;

class EstadoUnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $us = new EstadoUnidad();
        $us->nombre = "Buen Estado";
        $us->descripcion = "Esta en funcionamiento se ve bien y funciona normal.";
        $us->save();

        $us = new EstadoUnidad();
        $us->nombre = "Deteriorada";
        $us->descripcion = "Esta en funcionamiento pero, necesita algunas reparaciones";
        $us->save();

        $us = new EstadoUnidad();
        $us->nombre = "Descompuesta";
        $us->descripcion = "La unidad no esta en uso, tiene problemas mecánicos";
        $us->save();

    }
}
