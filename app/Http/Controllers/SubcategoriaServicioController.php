<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CategoriaServicio;
use App\Models\SubcategoriaServicio;

class SubcategoriaServicioController extends Controller
{
    public function index()
    {

        return view('subcategoria/SubcategoriaServicio');
    }
    public function create(Request $request)
    {

        $validateData = $request->validate([
            'subcategoria' => 'required|string|max:255',
            'id_categoria' => 'required',
            'estado' => 'required'
        ]);

        $SubcategoriaServicio = SubcategoriaServicio::create([
            'subcategoria' => $validateData['subcategoria'],
            'descripcion' => $request->descripcion,
            'id_categoria' => $validateData['id_categoria'],
            'estado' => $validateData['estado'],
        ]);

        return response()->json([
            'mensaje' => 'SubCategoria registrada exitosamente.'
        ], 200);
    }
    public function listJson()
    {

        return SubcategoriaServicio::all();
    }
    public function list()
    {

        $list = SubcategoriaServicio::all();
        return datatables($list)
            ->addColumn('btn', '<button class="btn btn-success" onclick="find({{$id}})"><i class="fas fa-edit"></i></button>
                           <button class="btn btn-danger" onclick="remove({{$id}})"><i class="fas fa-trash-alt"></i></button>')
            ->rawColumns(['btn'])
            ->make(true);
    }

    public function find(Request $request)
    {
        return SubcategoriaServicio::find($request->id);
    }

    public function update(Request $request)
    {

        $validateData = $request->validate([
            'id' => 'required',
            'subcategoria' => 'required|string|max:255',
            'id_categoria' => 'required',
            'estado' => 'required'
        ]);

        $SubcategoriaServicio = SubcategoriaServicio::findOrFail($request->id);

        if (!$SubcategoriaServicio) {
            return response()->json([
                'mensaje' => 'El registro no existe'
            ], 401);
        }

        $SubcategoriaServicio->subcategoria      = $validateData['subcategoria'];
        $SubcategoriaServicio->descripcion = $request->descripcion;
        $SubcategoriaServicio->id_categoria      = $validateData['id_categoria'];
        $SubcategoriaServicio->estado      = $validateData['estado'];
        $SubcategoriaServicio->save();

        return response()->json([
            'mensaje' => 'SubCategoria modificada exitosamente.'
        ], 200);
    }

    public function subcategorias(Request $request)
    {
        return SubcategoriaServicio::where('estado', true)->where('id_categoria', $request->id)->get();
    }
    public function delete(Request $request)
    {

        try {
            $subcategoria = SubcategoriaServicio::find($request->id);
            $subcategoria->delete();

            return response()->json([
                'mensaje' => 'Categoria eliminada correctamente'
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'El registro está en uso'
            ], 401);
        }
    }
}
