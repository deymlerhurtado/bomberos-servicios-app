@extends('layouts.app')
@section('content')
<html>
   <head>
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      <script type="text/javascript">
         google.charts.load('current', {'packages':['corechart']});
         google.charts.setOnLoadCallback(drawChart);
         
         var analytics = <?php echo $puestos; ?>
         
         function drawChart() {
         
           var data = google.visualization.arrayToDataTable(analytics);
         
           var options = {
             title: '',
             pieHole: 0.4,
           };
         
           var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
         
           chart.draw(data, options);
         }
         
  
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart2);
      var analytics2 = <?php echo $estado_bomberos; ?>

      function drawChart2() {

        var data = google.visualization.arrayToDataTable(analytics2);

        var options = {
          title: '',
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }

         
         google.charts.load('current', {'packages':['corechart']});
         google.charts.setOnLoadCallback(drawChart3);
         
         var analytics3 = <?php echo $adquisicion; ?>
         
         function drawChart3() {
         
           var data = google.visualization.arrayToDataTable(analytics3);
         
           var options = {
             title: '',
             pieHole: 0.4,
             is3D: true,
           };
         
           var chart = new google.visualization.PieChart(document.getElementById('donutchart3'));
         
           chart.draw(data, options);
         }

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart4);
      var analytics4 = <?php echo $estado_unidades; ?>

      function drawChart4() {

        var data = google.visualization.arrayToDataTable(analytics4);

        var options = {
          title: '',
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material4'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
         
         
         
      </script>
   </head>
   <body>


   <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h2 class=" text-gray-900 mb-2">Informe Gráfico</h2>
                        </div>


                        <div class="row">
         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
               <div class="card-body">
                  <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                           Puestos Registrados
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $puesto; ?></div>
                     </div>
                     <div class="col-auto">
                        <img class="img-fluid" src="{{ asset('img/puesto.png') }}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
               <div class="card-body">
                  <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                           Categorías Registradas
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $categoria; ?></div>
                     </div>
                     <div class="col-auto">
                        <img class="img-fluid" src="{{ asset('img/categoria.png') }}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
               <div class="card-body">
                  <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                           Formas de Aviso Registrados
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $aviso; ?></div>
                     </div>
                     <div class="col-auto">
                        <img class="img-fluid" src="{{ asset('img/aviso.png') }}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
      <h6><strong>Informe de Bomberos</strong></h6>
 
      <div class="row">


      <div class="col-md-12 col-xl-2">
                    <hr>
                    <center>
                     <img class="img-fluid" src="{{ asset('img/bombero.png') }}">
                     <div class="h5 mb-2 font-weight-bold text-primary text-uppercase mb-1">Bomberos Registrados</div>
                     <div class="h3 mb-2 font-weight-bold text-gray-800"><?php echo $totalbomberos; ?> </div>
                    </center>
                    <hr>  
      </div>

      <div class="col-md-12 col-xl-5">
         <div class="card mb-3">
            <div class="card-img-top" id="donutchart" style="width: 100%; height: auto;"></div>
            <div class="card-body">
            <h6><strong>Bomberos Registrados por Puesto</strong></h6>
            </div>
         </div>
      </div>

      <div class="col-md-12 col-xl-5">
         <div class="card mb-3">
            <div class="graph">
            <div class="card-img-top" id="columnchart_material" style="width: 100%; height: auto;"></div>
            </div>
            <div class="card-body">
            <h6><strong>Estado de Bomberos Registrados</strong></h6>
            </div>
         </div>
      </div>

    </div>
    
   <h6><strong>Informe de Unidades</strong></h6>
   <div class="row">
      <div class="col-md-12 col-xl-2">
                    <hr>
                    <center>
                     <img class="img-fluid" src="{{ asset('img/unidad.png') }}">
                     <div class="h5 mb-2 font-weight-bold text-primary text-uppercase mb-1">Unidades Registradas</div>
                     <div class="h3 mb-2 font-weight-bold text-gray-800"><?php echo $unidades; ?> </div>
                    </center>
                    <hr>  
      </div>

      <div class="col-md-12 col-xl-5">
         <div class="card mb-3">
            <div class="card-img-top" id="donutchart3" style="width: 100%; height: auto;"></div>
            <div class="card-body">
            <h6><strong>Método de Adquisición</strong></h6>
            </div>
         </div>
      </div>

      <div class="col-md-12 col-xl-5">
         <div class="card mb-3">
            <div class="card-img-top" id="columnchart_material4" style="width: 100%; height: auto;"></div>
            <div class="card-body">
            <h6><strong>Estado de Unidades Registradas</strong></h6>
            </div>
         </div>
      </div>

   </div>

   </div>
   </div>
   </div>
   </div>
   </div>

   </body>
</html>
@endsection