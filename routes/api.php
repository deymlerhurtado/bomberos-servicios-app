<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriaServicioController;
use App\Http\Controllers\FormaAvisoController;
use App\Http\Controllers\PuestoBomberosController;
use App\Http\Controllers\UnidadServicioController;
use App\Http\Controllers\BomberosController;
use App\Http\Controllers\ChartsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RolController;
use App\Http\Controllers\ServicioController;
use App\Http\Controllers\SubcategoriaServicioController;
use App\Http\Controllers\SubCategoriaSubCategoriaController;
use App\Http\Controllers\UnidadController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/categoriaServicio/list',[CategoriaServicioController::class, 'list']);
Route::post('/categoriaServicio/create',[CategoriaServicioController::class, 'create']);
Route::get('/categoriaServicio/find',[CategoriaServicioController::class, 'find']);
Route::put('/categoriaServicio/update',[CategoriaServicioController::class, 'update']);
Route::get('/categoriaServicio/categorias',[CategoriaServicioController::class, 'categorias']);
Route::delete('/categoriaServicio/delete',[CategoriaServicioController::class, 'delete']);

Route::get('/formaAvisos/list',[FormaAvisoController::class, 'list']);
Route::post('/formaAvisos/create',[FormaAvisoController::class, 'create']);
Route::get('/formaAvisos/find',[FormaAvisoController::class, 'find']);
Route::put('/formaAvisos/update',[FormaAvisoController::class, 'update']);
Route::delete('/formaAvisos/delete',[FormaAvisoController::class, 'delete']);

Route::get('/unidad/listService',[UnidadController::class, 'listService']);
Route::post('/unidad/create',[UnidadController::class, 'create']);
Route::get('/unidad/find/{id}',[UnidadController::class, 'find']);
Route::post('/unidad/update',[UnidadController::class, 'update']);
Route::delete('/unidad/delete',[UnidadController::class, 'delete']);
Route::get('/unidad/metodoAdquisicion',[UnidadController::class, 'metodoAdquisicion']);
Route::get('/unidad/estado',[UnidadController::class, 'estadoUnidad']);

//puestoBomberos
Route::get('/puestoBomberos/list',[PuestoBomberosController::class, 'list']);
Route::get('/puestoBomberos/listJson',[puestoBomberosController::class, 'listJson']);
Route::post('/puestoBomberos/create',[PuestoBomberosController::class, 'create']);
Route::get('/puestoBomberos/find',[PuestoBomberosController::class, 'find']);
Route::put('/puestoBomberos/update',[PuestoBomberosController::class, 'update']);
Route::delete('/puestoBomberos/delete',[PuestoBomberosController::class, 'delete']);

//Bomberos route
Route::post('/bomberos/create',[BomberosController::class, 'create']);
Route::get('/bomberos/find',[BomberosController::class, 'find']);
Route::post('/bomberos/update',[BomberosController::class, 'update']);
Route::get('/bomberos/list',[BomberosController::class, 'list']);
Route::get('/bomberos/listJson',[BomberosController::class, 'listJson']);
Route::delete('/bomberos/delete',[BomberosController::class, 'delete']);
Route::get('/bomberos/serviciosJson',[BomberosController::class, 'serviciosJson']);



//subcategorias
Route::get('/SubcategoriaServicio/list',[SubcategoriaServicioController::class, 'list']);
Route::post('/SubcategoriaServicio/create',[SubcategoriaServicioController::class, 'create']);
Route::get('/SubcategoriaServicio/find',[SubcategoriaServicioController::class, 'find']);
Route::put('/SubcategoriaServicio/update',[SubcategoriaServicioController::class, 'update']);
Route::get('/SubcategoriaServicio/subcategorias',[SubcategoriaServicioController::class, 'subcategorias']);
Route::delete('/SubcategoriaServicio/delete',[SubcategoriaServicioController::class, 'delete']);

//subcategorias hija
Route::get('/SubcategoriaSubcategoriaServicio/list',[SubCategoriaSubCategoriaController::class, 'list']);
Route::post('/SubcategoriaSubcategoriaServicio/create',[SubCategoriaSubCategoriaController::class, 'create']);
Route::get('/SubcategoriaSubcategoriaServicio/find',[SubCategoriaSubCategoriaController::class, 'find']);
Route::put('/SubcategoriaSubcategoriaServicio/update',[SubCategoriaSubCategoriaController::class, 'update']);
Route::get('/SubcategoriaSubcategoriaServicio/subcategorias',[SubCategoriaSubCategoriaController::class, 'subcategorias']);
Route::delete('/SubcategoriaSubcategoriaServicio/delete',[SubCategoriaSubCategoriaController::class, 'delete']);


//form servicios
Route::get('/servicios/formaAvisos',[FormaAvisoController::class, 'formaAvisos']);
Route::get('/servicios/bomberos',[BomberosController::class, 'bomberos']);//telefonista_turno
Route::get('/servicios/unidades',[UnidadController::class, 'unidades']);//telefonista_turno
Route::get('/servicios/bomberosPiloto',[BomberosController::class, 'bomberosPiloto']);//telefonista_turno
Route::post('/servicios/create',[ServicioController::class, 'create']);//telefonista_turno
Route::post('/servicios/edit',[ServicioController::class, 'edit']);
Route::get('/servicios/listaResumen',[ServicioController::class, 'ListResumen']);
Route::get('/servicios/listJson',[ServicioController::class, 'ListJson']);
Route::get('/servicios/JsonPacientes',[ServicioController::class, 'JsonPacientes']);


//api para los reportes
//para el count de categorias y subcategorías
Route::get('/categoriaServicio/listSubcategoria',[CategoriaServicioController::class, 'listSubcategoria']);
//para el reporte de servicios por unidad
Route::get('/Serviciounidad/listServicioUnidad',[ServicioController::class, 'listServicioUnidad']);
//para el reporte de servicios por unidad y por mes
Route::get('/Serviciounidadmes/listunidadmes',[ServicioController::class, 'listunidadmes']);
//para el reporte de servicios por categoria y por mes
Route::get('/Serviciocategoriames/listcategoriames',[ServicioController::class, 'listcategoriames']);
//para el reporte de servicios por sub-categoria y por mes
Route::get('/Serviciosubcategoriames/listsubcategoriames',[ServicioController::class, 'listsubcategoriames']);

//auth

Route::get('/roles', [AuthController::class, 'rol']);
Route::post('/registerUser', [AuthController::class, 'register']);
Route::get('/auth/list', [AuthController::class, 'list']);
Route::get('/auth/find', [AuthController::class, 'find']);
Route::put('/auth/update', [AuthController::class, 'update']);
Route::delete('/auth/delete', [AuthController::class, 'delete']);