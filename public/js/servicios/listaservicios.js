
let pacientes = [];
let bombero_reporte = "";

$(document).ready(function () {
    selectPiloto();
    selectFormaAviso();
    selectBomberos();
    selectFormaAviso();
    selectCategorias();
    selectUnidades();

    if ($.fn.dataTable.isDataTable('#tabla')) {
    } else {
        tabla();
    }

    $("#form").on('submit', function (evt) {
        evt.preventDefault();

        if ($("#form")[0].checkValidity() == true) {
            editarServicio();
        } else {
            console.log("Form incorrecto")

        }
    });
});

function tabla() {
    $('#tabla').DataTable({

        autoWidth: false,
        "processing": true,
        "serverSide": true,
        "order": [[0, "desc"]],
        "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "todo"]],
        "ajax": "../api/servicios/listaResumen",
        "columns": [
            { data: 'ID' },
            { data: 'no_reporte' },
            { data: 'fecha' },
            { data: 'pacientes' },
            { data: 'placa' },
            { data: 'hora_salida' },
            { data: 'km_recorrido' },
            { data: 'piloto' },
            { data: 'categoria' },
            { data: 'direccion_traslado' },
            { data: 'usuario' },
            { data: 'btn' },
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
            buttons: {
                print: 'imprimir'
            }

        }
    });
}
function see(id) {
    let _url = `../api/servicios/listJson?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {

            $('#id').text(response.id);
            $('#no_reporte').text(response.no_reporte);
            $('#hora').text(response.hora_redaccion);
            $('#fecha').text(response.fecha);
            $('#codigo').text(response.codigo);
            $('#direccion_traslado').text(response.direccion_traslado);
            $('#traslado').text(response.traslado);
            $('#telefono').text(response.telefono);
            $('#telefonista_turno').text(response.telefonista);
            $('#telefono').text(response.telefono);
            $('#forma_abiso').text(response.forma_aviso);
            //unidades
            $('#unidad').text(response.unidad);
            $('#placa').text(response.placa);
            $('#km_entrada').text(response.kilometraje_entrada);
            bombero_reporte = "";
            bombero_reporte = response.reportador;
            $('#km_salida').text(response.kilometraje_salida);
            $('#km_recorrido').text(response.kilometros_recorridos);
            $('#hora_salida').text(response.hora_salida);
            $('#hora_entrada').text(response.hora_entrada);
            $('#piloto').text(response.piloto);
            ayudantes(response.id);
            atendidos(response.id);
            $('#observaciones').text(response.observaciones);


            $("#titulo").text('Detalle del servicio');
            $("#modal").modal('show');
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function ayudantes(id) {
    let _url = `../api/bomberos/serviciosJson?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuesta = response;
            let html = '';
            respuesta.forEach(respuestas => {
                html += `
                    <tr>
                    <td>${respuestas.nombre}</td>
                    <td>${respuestas.apellido}</td>
                    <td>${respuestas.dpi}</td>
                    <td>${respuestas.telefono}</td>
                    <td>${respuestas.no_registro}</td>
                    <td>${respuestas.direccion}</td>
                    <td>${respuestas.puesto}</td>
                    
                    </tr>
                `
            });
            $('#ayudantes').html(html);
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function atendidos(id) {
    let _url = `../api/servicios/JsonPacientes?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuesta = response;
            let html = '';
            pacientes = [];
            pacientes.push(respuesta);
            respuesta.forEach(respuestas => {
                html += `
                    <tr>
                    <td>${respuestas.nombre_paciente}</td>
                    <td>${respuestas.edad}</td>
                    <td>${respuestas.sexo}</td>
                    <td>${respuestas.direccion_paciente}</td>                    
                    </tr>
                `
            });
            $('#atendidos').html(html);
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function find(id) {
    let _url = `../api/servicios/listJson?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {

            $('#idu').val(response.id);
            $('#no_reporteU').val(response.no_reporte);
            $('#hora_redaccionu').val(response.hora_redaccion);
            $('#fechau').val(response.fecha);
            $('#codigou').val(response.codigo);
            $('#direccion_trasladou').val(response.direccion_traslado);
            $('#trasladou').val(response.traslado);
            $('#telefonou').val(response.telefono);
            $('#id_telefonista_turnou').val(response.id_telefonista_turno);
            $('#telefonou').val(response.telefono);
            $('#id_forma_avisou').val(response.id_forma_aviso);
            //unidades
            $('#id_unidadu').val(response.id_unidad);
            $('#kilometraje_entradau').val(response.kilometraje_entrada);
            $('#kilometraje_salidau').val(response.kilometraje_salida);
            $('#hora_salidau').val(response.hora_salida);
            $('#hora_entradau').val(response.hora_entrada);
            $('#id_pilotou').val(response.id_piloto);
            $('#id_categoriau').val(response.id_categoria);
            selectSubCategorias(response.id_categoria, response.id_subcategoria);
            setTimeout(() => {
                selectSubCategoriasSubCategorias(response.id_subcategoria, response.id_subcategoria_subcategoria);
            }, 500);
            $('#id_bombero_reporteu').val(response.id_bombero_reporte);
            $('#observacionesu').val(response.observaciones);
            $("#modalu").modal('show');
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectPiloto() {
    let _url = `../api/servicios/bomberosPiloto`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.nombres} ${respuesta.apellidos}</option>`
            });
            $('#id_pilotou').html(html);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectFormaAviso() {
    let _url = `../api/servicios/formaAvisos`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.forma}</option>`
            });
            $('#id_forma_avisou').html(html);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectBomberos() {
    let _url = `../api/servicios/bomberos`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.nombres} ${respuesta.apellidos}</option>`
            });
            $('#id_telefonista_turnou').html(html);
            $('#id_bombero_reporteu').html(html);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectUnidades() {
    let _url = `../api/servicios/unidades`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.numero}-${respuesta.placa}</option>`
            });
            $('#id_unidadu').html(html);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectCategorias() {
    let _url = `../api/categoriaServicio/categorias`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.categoria}</option>`
            });
            $('#id_categoriau').html(html);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectSubCategorias(id, id2) {
    let _url = `../api/SubcategoriaServicio/subcategorias?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.subcategoria}</option>`
            });
            $('#id_sub_categoriau').html(html);
            $('#id_sub_categoriau').val(id2);
            $('#id_subcategoria_subcategoriaU').html('<option value="" selected hidden>Elegir...</option>');

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function selectSubCategoriasSubCategorias(id, id2) {
    let _url = `../api/SubcategoriaSubcategoriaServicio/subcategorias?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.subcategoria}</option>`
            });
            $('#id_subcategoria_subcategoriaU').html(html);
            setTimeout(() => {
                $('#id_subcategoria_subcategoriaU').val(id2);
            }, 500);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
//editar servicio LOL
function editarServicio() {
    let id = $('#idu').val();
    let direccion_traslado = $('#direccion_trasladou').val();
    let traslado = $('#trasladou').val();
    let id_forma_aviso = $('#id_forma_avisou').val();
    let telefono = $('#telefonou').val();
    let id_telefonista_turno = $('#id_telefonista_turnou').val();
    let id_unidad = $('#id_unidadu').val();
    let hora_salida = $('#hora_salidau').val();
    let hora_entrada = $('#hora_entradau').val();
    let id_piloto = $('#id_pilotou').val();
    let fecha = $('#fechau').val();
    let kilometraje_salida = $('#kilometraje_salidau').val();
    let kilometraje_entrada = $('#kilometraje_entradau').val();
    let id_bombero_reporte = $('#id_bombero_reporteu').val();
    let observaciones = $('#observacionesu').val();
    let hora_redaccion = $('#hora_redaccionu').val();
    let codigo = $('#codigou').val();
    let id_categoria = $('#id_categoriau').val();
    let id_sub_categoria = $('#id_sub_categoriau').val();
    let no_reporte = $('#no_reporteU').val();
    let id_subcategoria_subcategoria = $('#id_subcategoria_subcategoriaU').val();
    let _url = `../api/servicios/edit`;
    let _token = $('meta[name="csrf-token"]').attr('content');

    Swal.fire({
        title: '¿Está seguro?',
        text: "Se editara el servicio",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'si, seguro!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: _url,
                type: 'post',

                data: {
                    id: id,
                    direccion_traslado: direccion_traslado,
                    traslado: traslado,
                    id_forma_aviso: id_forma_aviso,
                    telefono: telefono,
                    id_telefonista_turno: id_telefonista_turno,
                    id_unidad: id_unidad,
                    hora_salida: hora_salida,
                    hora_entrada: hora_entrada,
                    id_piloto: id_piloto,
                    fecha: fecha,
                    kilometraje_salida: kilometraje_salida,
                    kilometraje_entrada: kilometraje_entrada,
                    id_bombero_reporte: id_bombero_reporte,
                    observaciones: observaciones,
                    hora_redaccion: hora_redaccion,
                    codigo: codigo,
                    id_categoria: id_categoria,
                    id_subcategoria_subcategoria: id_subcategoria_subcategoria,
                    no_reporte: no_reporte
                },
                dataType: 'JSON',
                success: function (response) {
                    if (response) {
                        $('#tabla').DataTable().ajax.reload();
                        toastr.success("Modificado con exito");
                        document.getElementsByClassName('needs-validation')[0].classList.remove("was-validated");
                        $('#form').trigger('reset');
                        $("#modalu").modal('hide');
                    }
                },
                error: function (errormessage) {
                    console.log(errormessage.responseText);
                }
            });
        }
    });

}

function printReport() {
    window.jsPDF = window.jspdf.jsPDF
    // Extraemos el


    const doc = new jsPDF('p', 'pt', 'letter');
    const options = {
        background: 'white',
        scale: 3
    };
    var img = new Image()
    img.src = 'http://bomberoscoatepeque.com/servicios/public/img/logo.png';
    doc.addImage(img, 'png', 30, 20, 100, 100)

    doc.setFontSize(13);
    doc.setFont("courier", "bold");
    doc.text(450, 30, "Reporte");
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(450, 40, "No." + $('#no_reporte').text());
    doc.setFontSize(13);
    doc.setFont("courier", "bold");
    doc.text(40, 160, "Dirección de traslado: ");
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(220, 160, $('#direccion_traslado').text());
    doc.setFontSize(13);
    doc.setFont("courier", "bold");
    doc.text(40, 180, "Pacientes atendidos:");
    doc.autoTable({ html: '#pacientesAtendidosTable', startY: 190 });
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Se trasladó a: ",
        40,
        doc.lastAutoTable.finalY + 20
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#direccion_traslado').text(),
        150,
        doc.lastAutoTable.finalY + 20
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Forma de aviso",
        40,
        doc.lastAutoTable.finalY + 40
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#forma_abiso').text(),
        220,
        doc.lastAutoTable.finalY + 40
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Teléfono",
        40,
        doc.lastAutoTable.finalY + 60
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#telefono').text(),
        220,
        doc.lastAutoTable.finalY + 60
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Telefonista de turno",
        40,
        doc.lastAutoTable.finalY + 80
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#telefonista_turno').text(),
        220,
        doc.lastAutoTable.finalY + 80
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Unidad No: ",
        40,
        doc.lastAutoTable.finalY + 100
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#unidad').text(),
        220,
        doc.lastAutoTable.finalY + 100
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Hora de salida:  ",
        40,
        doc.lastAutoTable.finalY + 120
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#hora_salida').text(),
        220,
        doc.lastAutoTable.finalY + 120
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Hora de entrada:  ",
        300,
        doc.lastAutoTable.finalY + 120
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#hora_entrada').text(),
        500,
        doc.lastAutoTable.finalY + 120
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Nombre del piloto:  ",
        40,
        doc.lastAutoTable.finalY + 140
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#piloto').text(),
        220,
        doc.lastAutoTable.finalY + 140
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Fecha:  ",
        40,
        doc.lastAutoTable.finalY + 160
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#fecha').text(),
        220,
        doc.lastAutoTable.finalY + 160
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Kilometraje de salida:  ",
        40,
        doc.lastAutoTable.finalY + 180
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#km_salida').text(),
        220,
        doc.lastAutoTable.finalY + 180
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Kilometraje de entrada:  ",
        300,
        doc.lastAutoTable.finalY + 180
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#km_entrada').text(),
        500,
        doc.lastAutoTable.finalY + 180
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Bombero que hizo el reporte:  ",
        40,
        doc.lastAutoTable.finalY + 200
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        bombero_reporte,
        260,
        doc.lastAutoTable.finalY + 200
    );
    doc.setFontSize(13);
    doc.setFont("courier", "bold");
    doc.text(
        "Asistentes: ",
        40,
        doc.lastAutoTable.finalY + 220
    );
    doc.autoTable({ html: '#tablaAsistentes', startY: doc.lastAutoTable.finalY + 240 });

    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Observaciones:  ",
        40,
        doc.lastAutoTable.finalY + 20
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#observaciones').text(),
        220,
        doc.lastAutoTable.finalY + 20
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Kilometros recorridos:  ",
        40,
        doc.lastAutoTable.finalY + 40
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#km_recorrido').text(),
        220,
        doc.lastAutoTable.finalY + 40
    );

    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Hora de redacción del reporte:  ",
        40,
        doc.lastAutoTable.finalY + 60
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#hora').text(),
        280,
        doc.lastAutoTable.finalY + 60
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "Código:  ",
        40,
        doc.lastAutoTable.finalY + 80
    );
    doc.setFontSize(11);
    doc.setFont("courier", "normal");
    doc.text(
        $('#codigo').text(),
        220,
        doc.lastAutoTable.finalY + 80
    );
    doc.setFont("courier", "bold");
    doc.setFontSize(13);
    doc.text(
        "F.)_____________________ ",
        40,
        doc.lastAutoTable.finalY + 140
    );


    doc.save("Reporte"+$('#no_reporte').text()+$('#fecha').text()+".pdf");


}