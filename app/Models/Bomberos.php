<?php

namespace App;

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bomberos extends Model
{
    public function puesto_bomberos(){
        return $this->belongsTo('App\Puesto_bomberos','id_puesto');
    }

    protected $fillable = [
        'nombres',
        'apellidos',
        'id_puesto',
        'fecha_nacimiento',
        'dpi',
        'telefono',
        'no_registro',
        'email',
        'direccion',
        'estado',
    ];
}
