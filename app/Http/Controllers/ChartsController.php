<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\Bomberos;
use App\Models\Unidad;
use App\Models\CategoriaServicio;
use App\Models\PuestoBomberos;
use App\Models\Forma_aviso;

class ChartsController extends Controller
{
    public function index()
    {

        $data1 = DB::table('puesto_bomberos')
            ->join('bomberos', 'puesto_bomberos.id', '=', 'bomberos.id_puesto')
            ->select('puesto_bomberos.puesto as puesto', DB::raw("count(bomberos.id_puesto) as cantidad"))
            ->groupBy('puesto_bomberos.puesto')
            ->get();
        
        $arraydata1[] = ['Puestos', 'Cantidad'];

        foreach($data1 as $key => $value){
            $arraydata1[++$key] = [$value->puesto, $value->cantidad];
        }

        
        $data2 = DB::table('bomberos')
            ->select('bomberos.estado as estado', DB::raw("count(bomberos.estado) as cantidad"))
            ->groupBy('bomberos.estado')
            ->get();
        
        $arraydata2[] = ['Estado', 'Cantidad'];

        foreach($data2 as $key2 => $value2){
            if($value2->estado==0){
                $value2->estado = 'Inactivo';
            }
            if($value2->estado==1){
                $value2->estado = 'Activo';
            }
            $arraydata2[++$key2] = [$value2->estado, $value2->cantidad];
        }


        $data3 = DB::table('unidades')
            ->join('metodo_adquisiciones', 'metodo_adquisiciones.id', '=', 'unidades.id_metodo_adquisicion')
            ->select('metodo_adquisiciones.adquisicion as adquisicion', DB::raw("count(unidades.id_metodo_adquisicion) as cantidad"))
            ->groupBy('metodo_adquisiciones.adquisicion')
            ->get();
        
        $arraydata3[] = ['Método de Adquisición', 'Cantidad'];

        foreach($data3 as $key3 => $value3){
            $arraydata3[++$key3] = [$value3->adquisicion, $value3->cantidad];
        }



        $data4 = DB::table('unidades')
            ->join('estado_unidades', 'estado_unidades.id', '=', 'unidades.estado_id')
            ->select('estado_unidades.nombre as estado', DB::raw("count(unidades.estado_id) as cantidad"))
            ->groupBy('estado_unidades.nombre')
            ->get();
        
        $arraydata4[] = ['Estado', 'Cantidad'];

        foreach($data4 as $key4 => $value4){
            $arraydata4[++$key4] = [$value4->estado, $value4->cantidad];
        }

        $totalbomberos = Bomberos::all()->count();
        $unidades = Unidad::all()->count();
        $categoria = CategoriaServicio::all()->count();
        $puesto = PuestoBomberos::all()->count();
        $aviso = Forma_aviso::all()->count();

        $arrayfin = [
            'puestos' => json_encode($arraydata1),
            'estado_bomberos' => json_encode($arraydata2),
            'adquisicion' => json_encode($arraydata3),
            'totalbomberos' => $totalbomberos,
            'unidades' => $unidades,
            'estado_unidades' => json_encode($arraydata4),
            'categoria' => $categoria,
            'puesto' => $puesto,
            'aviso' => $aviso,
        ];   

        return view('charts/index', $arrayfin);
            
    }
}
