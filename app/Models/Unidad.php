<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'unidades';

    protected $fillable = [
        'numero',
        'placa',
        'modelo',
        'anio',
        'estado_id',
        'id_metodo_adquisicion',
        'foto',
    ];

}
