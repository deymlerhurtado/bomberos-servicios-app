<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MetodoAdquisicion;


class AdquisicionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $adquisicion = MetodoAdquisicion::create([
            'id' => 1,
            'adquisicion' => 'Donación'
        ]);
        
        $adquisicion = MetodoAdquisicion::create([
            'id' => 2,
            'adquisicion' => 'Compra'
        ]);
        
    }
}
