<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBomberosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puesto_bomberos', function (Blueprint $table) {
            $table->id();
            $table->string('puesto');
            $table->text('descripcion')->nullable();
            $table->boolean('estado');
            $table->timestamps();
        });

        Schema::create('bomberos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_puesto')->unsigned();//relacion to puestos
            $table->string('nombres');
            $table->string('apellidos');
            $table->date('fecha_nacimiento');
            $table->bigInteger('dpi')->nullable();
            $table->string('email')->nullable();
            $table->text('telefono');
            $table->text('no_registro');
            $table->text('direccion')->nullable();
            $table->boolean('estado');
            $table->date('created_at');
            $table->date('updated_at');
        });
        Schema::table('bomberos', function($table) {
            $table->foreign('id_puesto')->references('id')->on('puesto_bomberos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bomberos');
        Schema::dropIfExists('puesto_bomberos');

    }
}
