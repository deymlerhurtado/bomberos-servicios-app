@extends('layouts.app')
@section('content')
<div class="card-body p-0">
    <div class="py-2">
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Bomberos Municipales Departamentales</h1>
                            <h1 class="h4 text-gray-900 mb-4">Reporte Según Servicios Realizados por Mes</h1>
                        </div>
                        <table class="table table-bordered responsive bg-light" width="100%" cellspacing="0" id="tabla">
                            <thead>
                                <tr>
                                    <th>Categorias</th>
                                    <th>Mes</th>
                                    <th>Año</th>
                                    <th>Total_Sub.deSubcategoria</th>
                                    </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>








@endsection
@section('scrips')
<script src="{{ asset('js/Serviciocategoriames.js') }}" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
@endsection