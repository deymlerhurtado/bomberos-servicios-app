<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicioPaciente extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre_paciente',
        'direccion_paciente',
        'edad',
        'sexo',
        'id_servicio' ,
    ];
}
