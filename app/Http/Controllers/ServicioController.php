<?php

namespace App\Http\Controllers;

use App\Models\Servicio;
use App\Models\ServicioAsistente;
use App\Models\ServicioPaciente;
use App;
use DB;
use Illuminate\Http\Request;


class ServicioController extends Controller
{

    public function index()
    {
        return view('/servicios/index');
    }
    public function vista_listado()
    {
        return view('/servicios/listado');
    }
    public function create(Request $request)
    {
        $validateData = $request->validate([
            'direccion_traslado' => 'required',
            'traslado' => 'required',
            'id_forma_aviso' => 'required',
            'id_telefonista_turno' => 'required',
            'id_unidad' => 'required',
            'hora_salida' => 'required',
            'hora_entrada' => 'required',
            'id_piloto' => 'required',
            'fecha' => 'required',
            'kilometraje_salida' => 'required',
            'kilometraje_entrada' => 'required',
            'id_bombero_reporte' => 'required',
            'hora_redaccion' => 'required',
            'codigo' => 'required',
            'id_subcategoria_subcategoria' => 'required',
            'no_reporte' => 'required',
        ]);


        $servicio = Servicio::create([
            'direccion_traslado' => $validateData['direccion_traslado'],
            'traslado' => $validateData['traslado'],
            'id_forma_aviso' => $validateData['id_forma_aviso'],
            'telefono' => $request->telefono,
            'id_telefonista_turno' => $validateData['id_telefonista_turno'],
            'id_unidad' => $validateData['id_unidad'],
            'hora_salida' => $validateData['hora_salida'],
            'hora_entrada' => $validateData['hora_entrada'],
            'id_piloto' => $validateData['id_piloto'],
            'fecha' => $validateData['fecha'],
            'kilometraje_salida' => $validateData['kilometraje_salida'],
            'kilometraje_entrada' => $validateData['kilometraje_entrada'],
            'id_bombero_reporte' => $validateData['id_bombero_reporte'],
            'observaciones' => $request->observaciones,
            'kilometros_recorridos' => $validateData['kilometraje_entrada'] - $validateData['kilometraje_salida'],
            'hora_redaccion' => $validateData['hora_redaccion'],
            'codigo' => $validateData['codigo'],
            'id_subcategoria_subcategoria' => $validateData['id_subcategoria_subcategoria'],
            'no_reporte' => $validateData['no_reporte'],
            'id_usuario' => $request->id_usuario,
        ]);

        $idServicio = $servicio->id;
        $asistentes =  $request->asistentes;
        $pacientes =  $request->pacientes;

        if ($asistentes != null || $asistentes != "") {

            if (count($asistentes) > 0) {
                foreach ($asistentes as $items) {

                    if ($items != null || $items != "") {
                        $servicio = ServicioAsistente::create([
                            'id_servicio' => $idServicio,
                            'id_asistente' => $items
                        ]);
                    }
                }
            }
        }
        if ($pacientes != null || $pacientes != "") {
            if (count($pacientes) > 0) {
                foreach ($pacientes as $items) {

                    if ($items != null || $items != "") {
                        $servicio = ServicioPaciente::create([
                            'nombre_paciente' => $items['nombre_paciente'],
                            'direccion_paciente' => $items['direccion_paciente'],
                            'edad' => $items['edad'],
                            'sexo' => $items['sexo'],
                            'id_servicio' => $idServicio,
                        ]);
                    }
                }
            }
        }

        return response()->json([
            'mensaje' => 'Categoria registrada exitosamente.'
        ], 200);
    }
    //editar servicio de 
    public function edit(Request $request)
    {

        $validateData = $request->validate([
            'id' => 'required',
            'direccion_traslado' => 'required',
            'traslado' => 'required',
            'id_forma_aviso' => 'required',
            'id_telefonista_turno' => 'required',
            'id_unidad' => 'required',
            'hora_salida' => 'required',
            'hora_entrada' => 'required',
            'id_piloto' => 'required',
            'fecha' => 'required',
            'kilometraje_salida' => 'required',
            'kilometraje_entrada' => 'required',
            'id_bombero_reporte' => 'required',
            'hora_redaccion' => 'required',
            'codigo' => 'required',
            'no_reporte' => 'required',
            'id_categoria' => 'required',
            'id_subcategoria_subcategoria' => 'required',
        ]);

        $edit = Servicio::findOrFail($request->id);


        $edit->direccion_traslado = $validateData['direccion_traslado'];
        $edit->traslado = $validateData['traslado'];
        $edit->id_forma_aviso = $validateData['id_forma_aviso'];
        $edit->telefono = $request->telefono;
        $edit->id_telefonista_turno = $validateData['id_telefonista_turno'];
        $edit->id_unidad = $validateData['id_unidad'];
        $edit->hora_salida = $validateData['hora_salida'];
        $edit->hora_entrada = $validateData['hora_entrada'];
        $edit->id_piloto = $validateData['id_piloto'];
        $edit->fecha = $validateData['fecha'];
        $edit->kilometraje_salida = $validateData['kilometraje_salida'];
        $edit->kilometraje_entrada = $validateData['kilometraje_entrada'];
        $edit->id_bombero_reporte = $validateData['id_bombero_reporte'];
        $edit->observaciones = $request->observaciones;
        $edit->kilometros_recorridos = $validateData['kilometraje_entrada'] - $validateData['kilometraje_salida'];
        $edit->hora_redaccion = $validateData['hora_redaccion'];
        $edit->codigo = $validateData['codigo'];
        $edit->id_subcategoria_subcategoria = $validateData['id_subcategoria_subcategoria'];
        $edit->no_reporte = $validateData['no_reporte'];

        $edit->save();

        return response()->json([
            'mensaje' => 'modificado.'
        ], 200);
    }
    public function ListResumen()
    {
        $consulta = DB::select('SELECT S.id AS ID, count(SP.id_servicio) as pacientes, S.fecha AS fecha, U.placa AS placa, S.hora_salida AS hora_salida, S.kilometros_recorridos AS km_recorrido,
        CONCAT(PIL.nombres, " ",PIL.apellidos) as piloto, S.no_reporte as no_reporte, UR.name as usuario, CA.subcategoria AS categoria, S.direccion_traslado AS direccion_traslado
        FROM servicios S
        INNER JOIN unidades U ON U.id = S.id_unidad
        INNER JOIN users UR ON UR.id = S.id_usuario
        INNER JOIN bomberos PIL ON PIL.id = S.id_piloto
        INNER JOIN subcategoria_subcategoria_servicios CA ON CA.id = S.id_subcategoria_subcategoria
        LEFT JOIN servicio_pacientes SP ON SP.id_servicio = S.id
        group by 1');

        return datatables($consulta)
            ->addColumn('btn', '<div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-outline-primary" >
                                <input type="radio" name="options" id="option1" autocomplete="off" onclick="see({{$ID}})">
                                <i class="far fa-eye"></i>
                            </label>
                            <label class="btn btn-outline-success">
                                <input type="radio" name="options" id="option2" autocomplete="off"  onclick="find({{$ID}})">
                                <i class="fas fa-edit"></i>
                            </label>
                        </div>')
            ->rawColumns(['btn'])
            ->make(true);
    }
    public function listJson(Request $request)
    {

        $consulta = DB::select(DB::RAW('SELECT S.*,SS.id AS id_subcategoria, C.id as id_categoria, U.numero AS unidad,CONCAT(T.nombres, " ", T.apellidos) as telefonista , U.placa AS placa, CONCAT(B.nombres, " ", B.apellidos) as piloto, CONCAT(R.nombres, " ", R.apellidos) as reportador, FA.forma as forma_aviso 
        from servicios S
        INNER JOIN unidades U ON U.id= S.id_unidad
        INNER JOIN subcategoria_subcategoria_servicios SSS ON SSS.id = S.id_subcategoria_subcategoria
        INNER JOIN subcategoria_servicios SS ON SS.id = SSS.id_subcategoria
        INNER JOIN categoria_servicios C ON C.id= SS.id_categoria
        INNER JOIN bomberos B ON B.id= S.id_piloto
        INNER JOIN bomberos T on T.id = S.id_telefonista_turno
        INNER JOIN bomberos R on R.id = S.id_bombero_reporte
        Join forma_avisos FA ON FA.id = S.id_forma_aviso
        where S.id =' . $request->id));

        return $consulta[0];
    }

    public function JsonPacientes(Request $request)
    {

        return ServicioPaciente::where('id_servicio', $request->id)->get();
    }


    //VISTA PARA EL REPORTE DE SERVICIOS POR UNIDAD
    public function indexreporteunidad()
    {
        return view('/reportes/Listadoservicioambulancia');
    }

    //envio de datos a la datatable con los datos del servicio de unidad

    public function listServicioUnidad()
    {


        $list = DB::select('select cat.identificador,cat.categoria, sub.subcategoria,subsubcat.subcategoria sub_sub_categoria, count(serv.id_subcategoria_subcategoria) as total
        from servicios serv 
        join subcategoria_subcategoria_servicios subsubcat on subsubcat.id = serv.id_subcategoria_subcategoria 
        join subcategoria_servicios sub on sub.id = subsubcat.id_subcategoria 
        join categoria_servicios cat on cat.id = sub.id_categoria
        group by sub.subcategoria, cat.categoria ,sub_sub_categoria,cat.identificador;');


        return datatables($list)

            ->make(true);
    }

    //VISTA PARA EL REPORTE DE UNIDADES Y MESES
    public function indexunidadmes()
    {
        return view('/reportes/Listadounidadmes');
    }

    //envio de datos a la datatable con los datos del servicio de unidad

    public function listunidadmes()
    {



        $lista = DB::select('select Tmeses.Mes, server.mes, server.numero, server.año, server.total  FROM
        (select 1 IdMes , "Enero"  Mes UNION 
        select 2  IdMes , "Febrero"  Mes UNION 
        select 3  IdMes , "Marzo"  Mes UNION 
        select 4  IdMes , "Abril"  Mes UNION 
        select 5  IdMes , "Mayo"  Mes UNION 
        select 6  IdMes , "Junio"  Mes UNION 
        select 7  IdMes , "Julio"  Mes UNION 
        select 8  IdMes , "Agosto"  Mes UNION 
        select 9  IdMes , "Septiembre"  Mes UNION 
        select 10  IdMes, "Octubre"  Mes UNION 
        select 11  IdMes, "Noviembre"  Mes UNION 
        select 12  IdMes, "Diciembre" Mes) Tmeses 
       left join (select month (s.fecha) mes, year (s.fecha) año, u.numero,count(s.id_unidad) as total from unidades u 
       left join servicios s on s.id_unidad = u.id group by u.numero,mes,año) as server
       on server.mes = Tmeses.idMes');




        return datatables($lista)

            ->make(true);
    }

    //VISTA PARA EL REPORTE DE categorias en cada mes
    public function indexcategoriames()
    {
        return view('/reportes/Listadocategoriames');
    }

    //envio de datos a la datatable con los datos de los servicios por categoria

    public function listcategoriames()
    {


        $listas = DB::select('select cat.categoria,monthname (serv.fecha) mes, 
        year (serv.fecha) año,count(serv.id_subcategoria_subcategoria) as total
            from servicios serv 
            join subcategoria_subcategoria_servicios subsubcat on subsubcat.id = serv.id_subcategoria_subcategoria 
            join subcategoria_servicios sub on sub.id = subsubcat.id_subcategoria 
            join categoria_servicios cat on cat.id = sub.id_categoria
            group by cat.categoria, mes, año;');


        return datatables($listas)

            ->make(true);
    }

    //VISTA PARA EL REPORTE DE categorias en cada mes
    public function indexsubcategoriames()
    {
        return view('/reportes/Listadosubcategoriames');
    }

    //envio de datos a la datatable con los datos de los servicios por categoria

    public function listsubcategoriames()
    {


        $listados = DB::select('select subsubcat.subcategoria clase_servicio,monthname (serv.fecha) mes, 
        year (serv.fecha) año,count(serv.id_subcategoria_subcategoria) as total
            from servicios serv 
            join subcategoria_subcategoria_servicios subsubcat on subsubcat.id = serv.id_subcategoria_subcategoria 
            join subcategoria_servicios sub on sub.id = subsubcat.id_subcategoria 
            join categoria_servicios cat on cat.id = sub.id_categoria
            group by clase_servicio, mes, año;');


        return datatables($listados)

            ->make(true);
    }
}
