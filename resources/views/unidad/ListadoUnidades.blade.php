@extends('layouts.app')
@section('content')
<div class="card-body p-0">
    <div class="py-2">
        <a href="#" onclick="modalCreate();" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">Agregar Unidad</span>
        </a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Unidades</h1>
                        </div>
                        <table class="responsive table table-bordered bg-light" width="100%" id="tabla">
                            <thead>
                                <tr>
                                    <th>Numero</th>
                                    <th>Foto</th>
                                    <th>Placa</th>
                                    <th>Modelo</th>
                                    <th>Año</th>
                                    <th>adquisición</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Modal Edit-->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="update producto" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title">Modificar unidad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" id="formUpdate" class="needs-validation" novalidate>
                    @csrf
                    <input type="hidden" value="" id="id" name="id" class="form-control">
                    <div class="input-group mb-3 required ">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Número</span>
                        </div>
                        <input type="text" class="form-control" id="numero" name="numero" required>
                        <div class="invalid-feedback">
                            porfavor ingrese un numero de unidad.
                        </div>

                    </div>
                    <div class="input-group mb-3 required ">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Placa</span>
                        </div>
                        <input type="text" class="form-control" id="placa" name="placa" required>
                        <div class="invalid-feedback">
                            porfavor ingrese la placa de la unidad.
                        </div>

                    </div>
                    <div class="input-group mb-3 required ">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Marca</span>
                        </div>
                        <input type="text" class="form-control" id="modelo" name="modelo" required>
                        <div class="invalid-feedback">
                            porfavor ingrese el modelo de la unidad.
                        </div>

                    </div>
                    <div class="form-group mb-3 required">
                        <span>Metodo de adquisición</span>
                        <select class="form-select form-control" id="id_metodo_adquisicion" name="id_metodo_adquisicion" aria-label="Default select example" required>
                        </select>
                        <div class="invalid-feedback">
                            Debe de seleccionar la forma de adquisición.
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <span>Estado</span>
                        <select class="form-select form-control" id="estado_id" aria-label="Default select example" required>
                            <option value="" disabled selected>Elegir..</option>
                        </select>
                        <div class="invalid-feedback">
                            Debe de seleccionar un estado.
                        </div>

                    </div>
                    <div class="input-group mb-3 required ">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Año</span>
                        </div>
                        <input type=" text" class="form-control" name="anio" id="anio" required />
                        <div class="invalid-feedback">
                            porfavor ingrese el año de la unidad.
                        </div>

                    </div>
                    <div class="form-row required">
                        <div class="col-md-6">
                            <div class="mx-auto" style="width: 220px;">
                                <img class="mb-5" width="220" id="vista_previa" src="../unidades/noimage.png" alt="Vista previa" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="custom-file">
                                        <input id="foto" name="foto" type="file" accept=".jpg,.jpeg,.png,.gif,.svg,.bmp" class="custom-file-input">
                                        <label class="custom-file-label" id="labelFoto" for="foto">Elija una foto</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button id="guardar" class="btn btn-primary">Guardar</button>
                    <button id="btneditar" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scrips')
<script src="{{ asset('js/Unidad.js') }}" defer></script>
<script src="{{ asset('js/previewImg.js') }}" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
@endsection