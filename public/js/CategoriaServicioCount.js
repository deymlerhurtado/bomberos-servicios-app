$(document).ready(function() {
    if ($.fn.dataTable.isDataTable('#tabla')) {} else {
        Listados();

    }

});

function Listados() {
    $("#tabla").dataTable().fnDestroy()


    let _url = `./api/categoriaServicio/listSubcategoria`;

    $('#tabla').DataTable({
        "lengthMenu": [
            [15, 25, 50, -1],
            [15, 25, 50, "todo"]
        ],
        autoWidth: false,
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: _url,
            data: {}
        },
        "columns": [{
                data: 'identificador'
            },
            {
                data: 'categoria'
            },
            {
                data: 'subcategoria'
            },
            {
                data: 'total'
            },
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'pdf',
                title: 'Reporte_Categoria_y_Total_SubCategorias'
            },
            {
                extend: 'excelHtml5',
                autoFilter: true,
                sheetName: 'Categoria_y_Total_SubCategorias',
                title: 'Reporte_Categoria_y_Total_SubCategorias'
            }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });
}