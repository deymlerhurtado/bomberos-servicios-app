@extends('layouts.app')
@section('content')
<div class="card-body p-0">
    <div class="py-2">
        <a href="#" onclick="modalCreate();" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
            </span>
            <span class="text">Nuevo Puesto</span>
        </a>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Puestos de Bomberos</h1>
                        </div>
                        <table class="table table-bordered responsive bg-light" width="100%" cellspacing="0" id="tablaB">
                            <thead>
                                <tr>
                                    <th>Puesto</th>
                                    <th>Descripción</th>
                                    <th>estado</th>
                                    <th>Acciones</th>
                                    </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Modal Edit-->
<div class="modal fade" id="modalPuestos" tabindex="-1" role="dialog" aria-labelledby="update producto" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title">Modificar Puesto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="" id="form" class="needs-validation" novalidate>
                    @csrf
                <input type="hidden" value="" id="id" name="id" class="form-control">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Puesto</span>
                        </div>
                        <input type="text" class="form-control" id="puesto" pattern="[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Descripción</span>
                        </div>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" maxlength="50" required>
                    </div>
                    <div class="form-group mb-3">
                        <select class="form-select form-control" id="estado" aria-label="Default select example" required>
                            <option value="" selected hidden>Estado del puesto</option>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                    <button id="add"  class="btn btn-primary">Agregar</button>
                    <button id="edit" class="btn btn-primary">Editar</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scrips')
    <script src="{{ asset('js/puestosBomberos.js') }}" defer></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="{{ asset('js/validator.js') }}"></script>
@endsection
