<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CategoriaServicio;
use App;
use DB;

class CategoriaServicioController extends Controller
{
    public function index()
    {


        return view('categoría/ListadoCategoriaServicio');
    }
    //index para la vista de las categorias y sus totales de subcategorías
    public function indexsubcategoria()
    {


        return view('categoría/ListadosubCategoriaServicio');
    }

    //envio de datos a la datatable del count que va al js CategoríaServicioCount.js

    public function listSubcategoria()
    {


        $categoriax = DB::select('select cat.identificador,cat.categoria,sub.subcategoria,count(subcs.id_subcategoria) as total
        from subcategoria_subcategoria_servicios subcs
        join subcategoria_servicios sub on sub.id = subcs.id_subcategoria
        join categoria_servicios cat on cat.id = sub.id_categoria 
        group by sub.subcategoria, cat.categoria, cat.identificador;');
        return datatables($categoriax)
            ->make(true);
    }

    public function create(Request $request)
    {

        $validateData = $request->validate([
            'categoria' => 'required|string|max:255',
            'identificador' => 'required',
            'estado' => 'required'
        ]);

        $categoriaServicio = CategoriaServicio::create([
            'categoria' => $validateData['categoria'],
            'identificador' => $validateData['identificador'],
            'descripcion' => $request->descripcion,
            'estado' => $validateData['estado'],
        ]);

        return response()->json([
            'mensaje' => 'Categoria registrada exitosamente.'
        ], 200);
    }

    public function list()
    {

        $list = CategoriaServicio::all();
        return datatables($list)
            ->addColumn('btn', '<button class="btn btn-success" onclick="find({{$id}})"><i class="fas fa-edit"></i></button>
        <button class="btn btn-danger" onclick="remove({{$id}})"><i class="fas fa-trash-alt"></i></button>')
            ->rawColumns(['btn'])
            ->make(true);
    }

    public function find(Request $request)
    {
        return CategoriaServicio::find($request->id);
    }

    public function update(Request $request)
    {

        $validateData = $request->validate([
            'id' => 'required',
            'categoria' => 'required|string|max:255',
            'identificador' => 'required',
            'estado' => 'required',
        ]);

        $categoriaServicio = CategoriaServicio::findOrFail($request->id);

        if (!$categoriaServicio) {
            return response()->json([
                'mensaje' => 'El registro no existe'
            ], 401);
        }

        $categoriaServicio->categoria      = $validateData['categoria'];
        $categoriaServicio->identificador      = $validateData['identificador'];
        $categoriaServicio->descripcion = $request->descripcion;
        $categoriaServicio->estado      = $validateData['estado'];
        $categoriaServicio->save();

        return response()->json([
            'mensaje' => 'Categoria modificada correctamente'
        ], 200);
    }

    public function categorias(Request $request)
    {
        $list = CategoriaServicio::where('estado', true)->get();
        return response()->json($list);
    }

    public function delete(Request $request)
    {

        try {
            $paciente = CategoriaServicio::find($request->id);
            $paciente->delete();

            return response()->json([
                'mensaje' => 'Categoria eliminada correctamente'
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'El registro está en uso'
            ], 401);
        }
    }
}
