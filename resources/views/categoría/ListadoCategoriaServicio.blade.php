@extends('layouts.app')
@section('content')
<div class="card-body p-0">
    <div class="py-2">
        <a href="#" onclick="modalCreate();" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">Agregar Categoría</span>
        </a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Categoría de Servicios</h1>
                        </div>
                        <table class="table table-bordered responsive bg-light" width="100%" id="tabla">
                            <thead>
                                <tr>
                                    <th>Identificador</th>
                                    <th>Categoría</th>
                                    <th>Descripción</th>
                                    <th>estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><br>

    <!--prueba de count-->

    <!--fin-->
</div>




<!--Modal Edit-->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="update producto" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title">Modificar categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" id="formUpdate" class="needs-validation" novalidate>
                    @csrf
                    <input type="hidden" value="" id="id" name="id" class="form-control">
                    <div class="input-group mb-3 required ">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Identificador</span>
                        </div>
                        <input type="text" class="form-control" id="identificador" name="identificador" required>
                        <div class="invalid-feedback">
                            porfavor ingrese un Identificador
                        </div>

                    </div>
                    <div class="input-group mb-3 required ">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Categoría</span>
                        </div>
                        <input type="text" class="form-control" id="categoria" name="categoria" required>
                        <div class="invalid-feedback">
                            porfavor ingrese una categoría.
                        </div>

                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Descripción</span>
                        </div>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" required>
                        <div class="invalid-feedback">
                            porfavor ingrese la descripción.
                        </div>

                    </div>
                    <div class="form-group mb-3">
                        <select class="form-select form-control" id="estado" aria-label="Default select example" required>
                            <option value="" disabled selected>Elegir..</option>
                            <option value="1">activo</option>
                            <option value="0">inactivo</option>
                        </select>
                        <div class="invalid-feedback">
                            Debe de seleccionar un estado.
                        </div>

                    </div>
                    <button id="guardar" class="btn btn-primary">Guardar</button>
                    <button id="btneditar" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scrips')
<script src="{{ asset('js/CategoriaServicio.js') }}" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
@endsection