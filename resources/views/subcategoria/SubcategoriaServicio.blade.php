@extends('layouts.app')
@section('content')
<div class="card-body p-0">
    <div class="py-2">
        <a href="#" onclick="modalCreate();" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
            </span>
            <span class="text">Agregar Subcategoria</span>
        </a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Subcategorias</h1>
                        </div>
                        <table class="table table-bordered responsive bg-light table-sm" width="100%" cellspacing="0" id="tabla">
                            <thead>
                                <tr>
                                    <th>Subcategoria</th>
                                    <th>Descripción</th>
                                    <th>Categoria</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Modal Edit-->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="update producto" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title">Modificar Subcategoria</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="" id="formUpdate" method="POST" class="needs-validation" novalidate>
                    @csrf
                <input type="hidden" value="" id="id" name="id" class="form-control">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Subcategoria</span>
                        </div>
                        <input type="text" class="form-control" id="subcategoria" name="subcategoria" required>
                        <div class="invalid-feedback">
                            porfavor ingrese una subcategoría.
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Descripción</span>
                        </div>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" required></input>
                        <div class="invalid-feedback">
                            porfavor ingrese la descripción.
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <select class="form-select form-control" id="id_categoria" name="id_categoria" aria-label="Default select example">
                            
                        </select>
                        <div class="invalid-feedback">
                            Debe de seleccionar una categoria.
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <select class="form-select form-control" id="estado" aria-label="Default select example">
                            <option selected>Estado</option>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                        <div class="invalid-feedback">
                            Debe de seleccionar un estado.
                        </div>
                    </div>
                    <button  id="guardar" class="btn btn-primary">Guardar</button>
                    <button  id="btneditar" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scrips')
    <script src="{{ asset('js/SubcategoriaServicio.js') }}" defer></script>
@endsection
