<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicioAsistente extends Model
{
    use HasFactory;
    protected $fillable = [
    'id_servicio' ,
    'id_asistente'
];
}
