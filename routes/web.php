<?php

use App\Models\UnidadServicio;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeChartsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/', HomeChartsController::class)->middleware('admin');

Route::get('/redirect', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
//return view
Route::get('/PuestoBomberos', [App\Http\Controllers\PuestoBomberosController::class, 'index'])->name('PuestoBomberos')->middleware('auth')->middleware('admin');
Route::get('/CategoríasServicios', [App\Http\Controllers\CategoriaServicioController::class, 'index'])->name('index-categoria')->middleware('auth')->middleware('admin');
Route::get('/UnidadServicio', [App\Http\Controllers\UnidadController::class, 'index'])->name('unidades.index')->middleware('auth')->middleware('admin');

//bomberos
Route::get('/bomberos', [App\Http\Controllers\BomberosController::class, 'index'])->name('index.bomberos')->middleware('auth')->middleware('admin');

Route::get('/FormaAviso', [App\Http\Controllers\FormaAvisoController::class, 'index'])->name('index-forma')->middleware('auth')->middleware('admin');
Route::get('/SubcategoriaServicios', [App\Http\Controllers\SubcategoriaServicioController::class,'index'])->name('index-subcategoria')->middleware('auth')->middleware('admin');
Route::get('/SubcategoriaSubcategoriaServicios', [App\Http\Controllers\SubCategoriaSubCategoriaController::class,'index'])->name('index-subcategoriaSubcategoria')->middleware('auth')->middleware('admin');
Route::get('/BomberoServicio', [App\Http\Controllers\BomberosController::class, 'index'])->name('bomberosServicio.index')->middleware('auth')->middleware('admin');
Route::get('/Usuarios', [App\Http\Controllers\AuthController::class, 'index'])->name('usuarios.index')->middleware('auth')->middleware('admin');

//graficas
Route::get('/Charts', [App\Http\Controllers\ChartsController::class, 'index'])->name('index-Charts')->middleware('auth')->middleware('admin');

//Servicios
Route::get('/servicios', [App\Http\Controllers\ServicioController::class, 'index'])->name('index.servicios')->middleware('auth');
Route::get('/servicios/listado', [App\Http\Controllers\ServicioController::class, 'vista_listado'])->name('listado.servicios')->middleware('auth')->middleware('admin');

//RUTAS PARA LOS REPORTES
//vista de categorias y subcategorías con el Count
Route::get('/CategoríasubServicios', [App\Http\Controllers\CategoriaServicioController::class, 'indexsubcategoria'])->name('indexsubcategoria-categoria')->middleware('auth')->middleware('admin');
//vista de reporte de servicios por unidad
Route::get('/ServiciosPorUnidad', [App\Http\Controllers\ServicioController::class, 'indexreporteunidad'])->name('indexreporteunidad-servicio')->middleware('auth')->middleware('admin');
//vista de reporte de servicios por unidad por meses
Route::get('/Serviciosunidadmes', [App\Http\Controllers\ServicioController::class, 'indexunidadmes'])->name('indexunidadmes-servicio')->middleware('auth')->middleware('admin');
//vista de reporte de los servicios realizados por  mes
Route::get('/Servicioscategoriames', [App\Http\Controllers\ServicioController::class, 'indexcategoriames'])->name('indexcategoriames-servicio')->middleware('auth')->middleware('admin');
//vista de reporte de los sub-servicios realizados por  mes
Route::get('/Serviciossubcategoriames', [App\Http\Controllers\ServicioController::class, 'indexsubcategoriames'])->name('indexsubcategoriames-servicio')->middleware('auth')->middleware('admin');

//bomberos
Route::get('/manual', [App\Http\Controllers\ManualController::class, 'index'])->name('manual.index')->middleware('auth')->middleware('admin');
Route::get('/manualdownload', [App\Http\Controllers\ManualController::class, 'download'])->name('manual.download')->middleware('auth')->middleware('admin');