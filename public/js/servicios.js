let pacientes = [];

$(document).ready(function () {
    selectFormaAviso();
    selectBomberos();
    selectUnidades();
    selectPiloto();
    selectCategorias();
    $("#form").on('submit', function (evt) {
        evt.preventDefault();

        if ($("#form")[0].checkValidity() == true) {
            CrearServicio();
        } else {
            console.log("Form incorrecto")

        }
    });
    $("#formPacientes").on('submit', function (evt) {
        evt.preventDefault();

        if ($("#formPacientes")[0].checkValidity() == true) {
            AggPaciente();
        } else {
            console.log("Form incorrecto")

        }
    });


});


function selectFormaAviso() {
    let _url = `./api/servicios/formaAvisos`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.forma}</option>`
            });
            $('#id_forma_aviso').html(html);
            $('.as1').selectpicker();

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectBomberos() {
    let _url = `./api/servicios/bomberos`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.nombres} ${respuesta.apellidos}</option>`
            });
            $('#id_telefonista_turno').html(html);
            $('#id_bombero_reporte').html(html);
            $('#asistentes').html(html);
            $('.asa').selectpicker();
            $('.asr').selectpicker();
            $('.ast').selectpicker();

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectUnidades() {
    let _url = `./api/servicios/unidades`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.numero}-${respuesta.placa}</option>`
            });
            $('#id_unidad').html(html);
            $('.as3').selectpicker();

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectPiloto() {
    let _url = `./api/servicios/bomberosPiloto`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.nombres} ${respuesta.apellidos}</option>`
            });
            $('#id_piloto').html(html);
            $('.as4').selectpicker();

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function RegistroPaciente() {
    $("#modal").modal("show");
}

function AggPaciente() {
    let nombre_paciente = $('#nombre_paciente').val();
    let direccion_paciente = $('#direccion_paciente').val();
    let edad = $('#edad').val();
    let sexo = $('#sexo').val();

    var paciente = { nombre_paciente: nombre_paciente, direccion_paciente: direccion_paciente, edad: edad, sexo: sexo }

    pacientes.push(paciente);

    ListPacientes();
    $('#formPacientes').trigger('reset');
    $("#modal").modal("hide");

}

function ListPacientes() {
    html = ''
    pacientes.forEach(paciente => {
        html += `<tr><th>${paciente.nombre_paciente}</th>
        <th>${paciente.direccion_paciente}</th>
        <th>${paciente.edad}</th>
        <th>${paciente.sexo}</th>
        <th> <button onclick="DeletePaciente(${pacientes.indexOf(paciente)})" class="btn btn-danger">Eliminar</button></th></tr>`
    });
    $('#bodyPacientes').html(html);
    console.log(pacientes);
}

function DeletePaciente(idx) {
    console.log(idx);
    pacientes.splice(idx, 1);

    ListPacientes();
}

function CrearServicio() {
    let direccion_traslado = $('#direccion_traslado').val();
    let traslado = $('#traslado').val();
    let id_forma_aviso = $('#id_forma_aviso').val();
    let telefono = $('#telefono').val();
    let id_telefonista_turno = $('#id_telefonista_turno').val();
    let id_unidad = $('#id_unidad').val();
    let hora_salida = $('#hora_salida').val();
    let hora_entrada = $('#hora_entrada').val();
    let id_piloto = $('#id_piloto').val();
    let fecha = $('#fecha').val();
    let kilometraje_salida = $('#kilometraje_salida').val();
    let kilometraje_entrada = $('#kilometraje_entrada').val();
    let id_bombero_reporte = $('#id_bombero_reporte').val();
    let observaciones = $('#observaciones').val();
    let hora_redaccion = $('#hora_redaccion').val();
    let codigo = $('#codigo').val();
    let id_subcategoria_subcategoria = $('#id_subcategoria_subcategoria').val();
    let asistentes = $('#asistentes').val();
    let id_usuario = $('#id_usuario').val();
    let no_reporte = $('#no_reporte').val();
    let pacientesForm = pacientes;

    console.log(pacientesForm);
    let _url = `./api/servicios/create`;
    let _token = $('meta[name="csrf-token"]').attr('content');

    if (pacientes.length < 1 || pacientes.length == 0) {

        Swal.fire({
            title: '¿Está seguro?',
            text: "No hay ningun paciente agregado",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'si, seguro!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: _url,
                    type: 'post',

                    data: {
                        direccion_traslado: direccion_traslado,
                        traslado: traslado,
                        id_forma_aviso: id_forma_aviso,
                        telefono: telefono,
                        id_telefonista_turno: id_telefonista_turno,
                        id_unidad: id_unidad,
                        hora_salida: hora_salida,
                        hora_entrada: hora_entrada,
                        id_piloto: id_piloto,
                        fecha: fecha,
                        kilometraje_salida: kilometraje_salida,
                        kilometraje_entrada: kilometraje_entrada,
                        id_bombero_reporte: id_bombero_reporte,
                        observaciones: observaciones,
                        hora_redaccion: hora_redaccion,
                        codigo: codigo,
                        id_subcategoria_subcategoria: id_subcategoria_subcategoria,
                        asistentes: asistentes,
                        pacientes: pacientesForm,
                        id_usuario: id_usuario,
                        no_reporte: no_reporte,
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        if (response) {
                            toastr.success("Creado con exito");
                            pacientes = [];
                            ListPacientes();
                            console.log(response);
                            $('#form').trigger('reset');
                            document.getElementsByClassName('formGeneral')[0].classList.remove("was-validated");

                        }
                    },
                    error: function (errormessage) {
                        console.log(errormessage.responseText);
                    }
                });
            }
        });

    } else {
        Swal.fire({
            title: '¿Está seguro?',
            text: "Se registrará el servicio",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'si, seguro!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: _url,
                    type: 'post',

                    data: {
                        direccion_traslado: direccion_traslado,
                        traslado: traslado,
                        id_forma_aviso: id_forma_aviso,
                        telefono: telefono,
                        id_telefonista_turno: id_telefonista_turno,
                        id_unidad: id_unidad,
                        hora_salida: hora_salida,
                        hora_entrada: hora_entrada,
                        id_piloto: id_piloto,
                        fecha: fecha,
                        kilometraje_salida: kilometraje_salida,
                        kilometraje_entrada: kilometraje_entrada,
                        id_bombero_reporte: id_bombero_reporte,
                        observaciones: observaciones,
                        hora_redaccion: hora_redaccion,
                        codigo: codigo,
                        id_subcategoria_subcategoria: id_subcategoria_subcategoria,
                        asistentes: asistentes,
                        pacientes: pacientesForm,
                        id_usuario: id_usuario,
                        no_reporte: no_reporte,
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        if (response) {
                            toastr.success("Creado con exito");
                            console.log(response);
                            pacientes = [];
                            ListPacientes();
                            $('#form').trigger('reset');
                            document.getElementsByClassName('formGeneral')[0].classList.remove("was-validated");
                        }
                    },
                    error: function (errormessage) {
                        console.log(errormessage.responseText);
                    }
                });
            }
        });
    }

}




function selectCategorias() {
    let _url = `./api/categoriaServicio/categorias`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.categoria}</option>`
            });
            $('#id_categoria').html(html);
            $('.as5').selectpicker();


        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function selectSubCategorias(id) {
    console.log(id);
    let _url = `./api/SubcategoriaServicio/subcategorias?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.subcategoria}</option>`
            });
            $('#id_sub_categoria').html(html);
            $('.as6').selectpicker('refresh');
            $('#id_subcategoria_subcategoria').html('<option value="" selected hidden>Elegir...</option>');
            $('.as7').selectpicker('refresh');
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function selectSubCategoriasSubCategorias(id) {
    console.log(id);
    let _url = `./api/SubcategoriaSubcategoriaServicio/subcategorias?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            let respuestas = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuestas.forEach(respuesta => {
                html += `<option value="${respuesta.id}">${respuesta.subcategoria}</option>`
            });
            $('#id_subcategoria_subcategoria').html(html);
            $('.as7').selectpicker('refresh');

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function minkilo(){
    let kilometraje_salida = $('#kilometraje_salida').val();
    document.getElementById("kilometraje_entrada").min = kilometraje_salida;
}