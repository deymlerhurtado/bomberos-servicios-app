$(document).ready(function() {
    Listado();
    selectAdquisicion();
    selectEstado();
    $("#formUpdate").on('submit', function(evt) {
        evt.preventDefault();

        if ($("#formUpdate")[0].checkValidity() == true) {

            let id = $('#id').val();
            console.log(id);
            if (id != 0 || id > 0) {
                update();
            } else {
                console.log("hola");
                CrearUnidad();
            }
        } else {
            console.log("Form incorrecto")

        }
    });
});

function modalCreate() {
    $("#formUpdate input:hidden").val('');

    $('#btneditar').hide();
    $('#title').text('Registrar');
    $('#guardar').show();
    $('#modal').modal('show');
}

function selectEstado() {
    let _url = `./api/unidad/estado`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function(response) {
            let respuesta = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuesta.forEach(metodo => {
                html += `<option value="${metodo.id}">${metodo.nombre}</option>`
            });
            $('#estado_id').html(html);

        },
        error: function(errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function selectAdquisicion() {
    let _url = `./api/unidad/metodoAdquisicion`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function(response) {
            let respuesta = response;
            let html = '`<option value="" selected hidden>Elegir...</option>`';
            respuesta.forEach(metodo => {
                html += `<option value="${metodo.id}">${metodo.adquisicion}</option>`
            });
            $('#id_metodo_adquisicion').html(html);

        },
        error: function(errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function CrearUnidad() {

    let numero = $('#numero').val();
    let placa = $('#placa').val();
    let modelo = $('#modelo').val();
    let id_metodo_adquisicion = $('#id_metodo_adquisicion').val();
    let estado_id = $('#estado_id').val();
    let anio = $('#anio').val();
    let foto = $('#foto')[0].files[0];



    const fb = new FormData();
    fb.append('numero', numero);
    fb.append('placa', placa);
    fb.append('modelo', modelo);
    fb.append('id_metodo_adquisicion', id_metodo_adquisicion);
    fb.append('estado_id', estado_id);
    fb.append('anio', anio);
    fb.append('foto', foto);


    let _url = `./api/unidad/create`;
    let _token = $('meta[name="csrf-token"]').attr('content');
    console.log(fb);
    $.ajax({
        url: _url,
        type: 'post',
        contentType: false,
        processData: false,
        data: fb,
        dataType: 'JSON',
        success: function(response) {
            if (response) {
                console.log(response);
                $('#formUpdate').trigger("reset");
                $('#tabla').DataTable().ajax.reload();
                $('#modal').modal('hide');
            }
        },
        error: function(errormessage) {
            console.log(errormessage.responseText);
        }
    });

}


function Listado() {
    let _url = `./api/unidad/listService`;

    $('#tabla').DataTable({
        autoWidth: false,
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: _url,
        },
        "columns": [{
                data: 'numero',
                class: 'all'
            },
            {
                data: 'img'
            },
            {
                data: 'placa',
                class: 'all'
            },
            {
                data: 'modelo'
            },
            {
                data: 'anio'
            },
            {
                data: 'adquisicion'
            },
            {
                data: 'estado_unidades'
            },
            {
                data: 'btn'
            }
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'pdf',
                title: 'Reporte_Unidades'
            },
            {
                extend: 'excelHtml5',
                autoFilter: true,
                sheetName: 'Exported data'
            }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

}

function find(id) {
    console.log(id);
    let _url = `./api/unidad/find/${id}`;

    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function(response) {
            let html = '';
            if (response) {
                $('#id').val(response.id);
                $('#numero').val(response.numero);
                $('#placa').val(response.placa);
                $('#modelo').val(response.modelo);
                $('#id_metodo_adquisicion').val(response.id_metodo_adquisicion);
                $('#estado_id').val(response.estado_id);
                $('#anio').val(response.anio);
                $("#vista_previa").attr('src', '../unidades/' + response.foto);
                $('#title').text('Editar');
                $('#guardar').hide();
                $('#btneditar').show();
                $("#modal").modal('show');

            }
        },
        error: function(errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function update() {
    let id = $('#id').val();
    let numero = $('#numero').val();
    let placa = $('#placa').val();
    let modelo = $('#modelo').val();
    let id_metodo_adquisicion = $('#id_metodo_adquisicion').val();
    let estado_id = $('#estado_id').val();
    let anio = $('#anio').val();
    let foto = $('#foto')[0].files[0];



    const fb = new FormData();
    fb.append('id', id);
    fb.append('numero', numero);
    fb.append('placa', placa);
    fb.append('modelo', modelo);
    fb.append('id_metodo_adquisicion', id_metodo_adquisicion);
    fb.append('estado_id', estado_id);
    fb.append('anio', anio);
    fb.append('foto', foto);

    let _url = `./api/unidad/update`;
    let _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: _url,
        type: 'post',
        contentType: false,
        processData: false,
        data: fb,
        dataType: 'JSON',
        success: function(response) {
            if (response) {
                console.log(response);
                $('#formUpdate').trigger("reset");
                $("#formUpdate input:hidden").val(' ');
                $('#tabla').DataTable().ajax.reload();

                $("#modal").modal('hide');

            }
        },
        error: function(errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function remove(id){
    Swal.fire({
        title: '¿esta seguro?',
        text: "¿seguro que quieres eliminar?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'si, seguro!'
      }).then((result) => {
        if (result.isConfirmed) {

            let _url = `./api/unidad/delete?id=${id}`;
            let _token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: _url,
            type: 'delete',
            data: {
            },
            dataType: 'JSON',
            success: function (response) {
                if (response) {
                    console.log(response);
                    $('#tabla').DataTable().ajax.reload();
                    toastr.success("eliminado");

                }
            },
            error: function (errormessage) {
                toastr.error("El registro está en uso");

                console.log(errormessage.responseText);
            }
        });
      
    }
  })
}
