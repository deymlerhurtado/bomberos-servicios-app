@extends('layouts.app')
@section('content')
<div class="card-body p-0">
    <div class="py-2">
        <a href="#" onclick="modalCreate();" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">Agregar Usuario</span>
        </a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-0">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Usuarios</h1>
                        </div>
                        <table class="table table-bordered responsive bg-light" width="100%" id="tabla">
                            <thead>
                                <tr>
                                    <th>Usuario</th>
                                    <th>Correo</th>
                                    <th>Rol</th>
                                    <th>Bombero</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><br>

    <!--prueba de count-->

    <!--fin-->
</div>




<!--Modal Edit-->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="update producto" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo">Modificar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form" class="needs-validation" novalidate>
                    @csrf
                    <input type="hidden" value="" id="id" name="id" class="form-control">
                    <div class="input-group mb-3 required ">
                        <div class="input-group-prepend">
                            <span for="inputcodigo" class="input-group-text">Usuario</span>
                        </div>
                        <input type="text" class="form-control" id="name" name="name" required>
                        <div class="invalid-feedback">
                            porfavor ingrese un usuario.
                        </div>

                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Correo</span>
                        </div>
                        <input type="email" class="form-control" id="email" name="email" required>
                        <div class="invalid-feedback">
                            debe de ingresar un correo valido.
                        </div>

                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Contraseña</span>
                        </div>
                        <input class="form-control" type="password" id="password" minlength="8" required>
                        <div class="input-group-prepend">
                            <a style="cursor: pointer;"><i class="fa fa-eye-slash" id="show_hide_password" aria-hidden="true"></i></a>
                        </div>
                        <div class="invalid-feedback">
                            La contraseña debe de tener por lo menos 8 dígitos.
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <select class="form-select form-control" id="estado" aria-label="Default select example" required>
                            <option value="" disabled selected>Elegir..</option>
                            <option value="1">activo</option>
                            <option value="0">inactivo</option>
                        </select>
                        <div class="invalid-feedback">
                            Debe de seleccionar un estado.
                        </div>
                    </div>
                    <div class="form-group md-3">
                        <label for="encargado">Rol *</label>
                        <select id="id_rol" class="form-select form-control" name="id_rol" required>
                        </select>
                    </div>
                    <div class="form-group md-3">
                        <label for="encargado">Bombero *</label>
                        <select id="id_bombero" class="form-select form-control" name="id_bombero" required>
                        </select>
                    </div>
                    <button id="add" class="btn btn-primary">Agregar</button>
                    <button id="edit" class="btn btn-primary">Editar</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scrips')
<script src="{{ asset('js/auth/usuario.js') }}" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
@endsection