$(document).ready(function() {
    if ($.fn.dataTable.isDataTable('#tabla')) {} else {
        Listados();

    }

});

function Listados() {
    $("#tabla").dataTable().fnDestroy()


    let _url = `./api/Serviciounidad/listServicioUnidad`;

    $('#tabla').DataTable({
        "lengthMenu": [
            [15, 25, 50, -1],
            [15, 25, 50, "todo"]
        ],
        autoWidth: false,
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: _url,
            data: {}
        },
        "columns": [{
                data: 'identificador'
            },
            {
                data: 'categoria'
            },
            {
                data: 'subcategoria'
            },
            {
                data: 'sub_sub_categoria'
            },
            {
                data: 'total'
            },
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'pdf',
                title: 'Bomberos_Municipales_Departamentales_Estadistica_General'
            },
            {
                extend: 'excelHtml5',
                autoFilter: true,
                sheetName: 'Estadistica_General',
                title: 'Bomberos_Municipales_Departamentales_Estadistica_General'
            }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });
}