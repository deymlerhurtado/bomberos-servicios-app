@extends('layouts.app')

@section('content')

<div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-puestobomber-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Puesto de Bomberos!</h1>
                            </div>
                            <form class="user">
                                <div class="form-group py-2">
                                    <input type="text" class="form-control " id="puesto"
                                        placeholder="nombre del puesto">
                                </div>
                                <div class="form-group py-2">
                                    <input type="text" class="form-control " id="descripcion"
                                        placeholder="descripcion del puesto">
                                </div>
                                <div class="form-group py-2">
                                    <select class="form-select form-control" aria-label="Default select example">
                                        <option selected>Estado del puesto</option>
                                        <option value="true">activo</option>
                                        <option value="false">inactivo</option>
                                    </select>
                                </div>
                                
                                <a href="" class="btn btn-danger btn-user btn-block py-2">
                                    registrar
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('scrips')
 <!-- 
     agregue sus crips aqui
    <script src="{{ asset('js/ejemplo/ejemplo.js') }}" defer></script>
-->
@endsection
