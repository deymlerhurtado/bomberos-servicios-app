$(document).ready(function() {
    if ($.fn.dataTable.isDataTable('#tabla')) {} else {
        Listados();

    }

});

function Listados() {
    $("#tabla").dataTable().fnDestroy()


    let _url = `./api/Serviciosubcategoriames/listsubcategoriames`;

    $('#tabla').DataTable({
        "lengthMenu": [
            [15, 25, 50, -1],
            [15, 25, 50, "todo"]
        ],
        autoWidth: false,
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: _url,
            data: {}
        },
        "columns": [{
                data: 'clase_servicio'
            },
            {
                data: 'mes'
            },
            {
                data: 'año'
            },
            {
                data: 'total'
            },
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'pdf',
                title: 'Estadística_Anual_de_Servicios_Durante_el_año'
            },
            {
                extend: 'excelHtml5',
                autoFilter: true,
                sheetName: 'Total_Anual',
                title: 'Estadística_Anual_de_Servicios_Durante_el_año'
            }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });
}