<?php

namespace App\Http\Controllers;
use App\Models\Servicio;
use App\Models\Bomberos;
use App\Models\ServicioPaciente;
use App\Models;
use DB;
use Illuminate\Http\Request;


class HomeChartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = Servicio::all()->count();
        $pacientes = ServicioPaciente::all()->count();
        $bomberos_activos = Bomberos::where('estado','=','1')->count();

        //segun sub-categoria
         $data1 = DB::table('subcategoria_subcategoria_servicios')
            ->join('servicios', 'subcategoria_subcategoria_servicios.id', '=', 'servicios.id_subcategoria_subcategoria')
            ->join('subcategoria_servicios', 'subcategoria_servicios.id', '=', 'subcategoria_subcategoria_servicios.id_subcategoria')
            ->select('subcategoria_servicios.subcategoria as servicio', DB::raw("count(servicios.id_subcategoria_subcategoria) as cantidad"))
            ->groupBy('subcategoria_servicios.subcategoria')
            ->get();
        
        $arraydata1[] = ['Servicio', 'Cantidad'];

        foreach($data1 as $key => $value){
            $arraydata1[++$key] = [$value->servicio, $value->cantidad];
        }


        

        //segun unidad
         $data2 = DB::table('unidades')
            ->join('servicios', 'unidades.id', '=', 'servicios.id_unidad')
            ->select('unidades.numero as unidad', DB::raw("count(servicios.id_unidad) as cantidad"))
            ->groupBy('unidades.numero')
            ->get();
        
        $arraydata2[] = ['Unidad', 'Cantidad'];

        foreach($data2 as $key2 => $value2){
            $arraydata2[++$key2] = [$value2->unidad, $value2->cantidad];
        }

        //segun sexo del paciente
        $data3= DB::table('servicio_pacientes')
            ->select('servicio_pacientes.sexo as sexo', DB::raw("count(servicio_pacientes.sexo) as cantidad"))
            ->groupBy('servicio_pacientes.sexo')
            ->get();
        
        $arraydata3[] = ['Sexo', 'Cantidad'];

        foreach($data3 as $key3 => $value3){
            $arraydata3[++$key3] = [$value3->sexo, $value3->cantidad];
        }

        //segun lugar de traslado del paciente
        $data4= DB::table('servicios')
            ->select('servicios.traslado as lugar', DB::raw("count(servicios.traslado) as cantidad"))
            ->groupBy('servicios.traslado')
            ->get();
        
        $arraydata4[] = ['Lugar de Traslado', 'Cantidad'];

        foreach($data4 as $key4 => $value4){
            $arraydata4[++$key4] = [$value4->lugar, $value4->cantidad];
        }

        
            
        //segun sub-sub-categoria
         $data5 = DB::table('subcategoria_subcategoria_servicios')
            ->join('servicios', 'subcategoria_subcategoria_servicios.id', '=', 'servicios.id_subcategoria_subcategoria')
            ->select('subcategoria_subcategoria_servicios.subcategoria as servicio', DB::raw("count(servicios.id_subcategoria_subcategoria) as cantidad"))
            ->groupBy('subcategoria_subcategoria_servicios.subcategoria')
            ->get();
        
        $arraydata5[] = ['Sub-Sub-Categoria', 'Cantidad'];

        foreach($data5 as $key => $value){
            $arraydata5[++$key] = [$value->servicio, $value->cantidad];
        }


        //segun categoria
        $data6 = DB::table('subcategoria_subcategoria_servicios')
        ->join('servicios', 'subcategoria_subcategoria_servicios.id', '=', 'servicios.id_subcategoria_subcategoria')
        ->join('subcategoria_servicios', 'subcategoria_servicios.id', '=', 'subcategoria_subcategoria_servicios.id_subcategoria')
        ->join('categoria_servicios', 'categoria_servicios.id', '=', 'subcategoria_servicios.id_categoria')
        ->select('categoria_servicios.categoria as servicio', DB::raw("count(servicios.id_subcategoria_subcategoria) as cantidad"))
        ->groupBy('categoria_servicios.categoria')
        ->get();
    
        $arraydata6[] = ['Sub-Categoria', 'Cantidad'];

    foreach($data6 as $key => $value){
        $arraydata6[++$key] = [$value->servicio, $value->cantidad];
    }



      $arrayfin = [
          'total_servicios' => json_encode($servicios),
          'total_pacientes' => json_encode($pacientes),
          'bomberos_activos' => json_encode($bomberos_activos),
          'categoria' => json_encode($arraydata1),
          'unidad' => json_encode($arraydata2),
          'sexo' => json_encode($arraydata3),
          'lugar' => json_encode($arraydata4),
          'subcategoria' => json_encode($arraydata5),
          'categoriasi' => json_encode($arraydata6),

      ];   

        return view('charts.estadistica', $arrayfin);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $date = $request->input('desde');
        $timestamp = strtotime($date);
        $new_date = date('Y-m-d',$timestamp );
        $date2 = $request->input('hasta');
        $timestamp2 = strtotime($date2);
        $new_date2 = date('Y-m-d',$timestamp2 );

        $servicios = Servicio::all()->whereBetween('fecha', [$new_date, $new_date2])->count();
        $pacientes = ServicioPaciente::all()->whereBetween('created_at', [$new_date, $new_date2])->count();
        $bomberos_activos = Bomberos::where('estado','=','1')->whereBetween('created_at', [$new_date, $new_date2])->count();

  
        //segun unidad
         $data2 = DB::table('unidades')
            ->join('servicios', 'unidades.id', '=', 'servicios.id_unidad')
            ->select('unidades.numero as unidad', DB::raw("count(servicios.id_unidad) as cantidad"))
            ->groupBy('unidades.numero')
            ->whereBetween('fecha', [$new_date, $new_date2])->get();
        
        $arraydata2[] = ['Unidad', 'Cantidad'];

        foreach($data2 as $key2 => $value2){
            $arraydata2[++$key2] = [$value2->unidad, $value2->cantidad];
        }

        //segun sexo del paciente
        $data3= DB::table('servicio_pacientes')
            ->select('servicio_pacientes.sexo as sexo', DB::raw("count(servicio_pacientes.sexo) as cantidad"))
            ->groupBy('servicio_pacientes.sexo')
            ->whereBetween('created_at', [$new_date, $new_date2])->get();
        
        $arraydata3[] = ['Sexo', 'Cantidad'];

        foreach($data3 as $key3 => $value3){
            $arraydata3[++$key3] = [$value3->sexo, $value3->cantidad];
        }

        //segun lugar de traslado del paciente
        $data4= DB::table('servicios')
            ->select('servicios.traslado as lugar', DB::raw("count(servicios.traslado) as cantidad"))
            ->groupBy('servicios.traslado')
            ->whereBetween('fecha', [$new_date, $new_date2])->get();
        
        $arraydata4[] = ['Lugar de Traslado', 'Cantidad'];

        foreach($data4 as $key4 => $value4){
            $arraydata4[++$key4] = [$value4->lugar, $value4->cantidad];
        }



        //segun categoria
          $data6 = DB::table('subcategoria_subcategoria_servicios')
          ->join('servicios', 'subcategoria_subcategoria_servicios.id', '=', 'servicios.id_subcategoria_subcategoria')
          ->join('subcategoria_servicios', 'subcategoria_servicios.id', '=', 'subcategoria_subcategoria_servicios.id_subcategoria')
          ->join('categoria_servicios', 'categoria_servicios.id', '=', 'subcategoria_servicios.id_categoria')
          ->select('categoria_servicios.categoria as servicio', DB::raw("count(servicios.id_subcategoria_subcategoria) as cantidad"))
          ->groupBy('categoria_servicios.categoria')
          ->whereBetween('fecha', [$new_date, $new_date2])->get();
      
          $arraydata6[] = ['Sub-Categoria', 'Cantidad'];

      foreach($data6 as $key => $value){
          $arraydata6[++$key] = [$value->servicio, $value->cantidad];
      }



        $arrayfin = [
            'total_servicios' => json_encode($servicios),
            'total_pacientes' => json_encode($pacientes),
            'bomberos_activos' => json_encode($bomberos_activos),
            'unidad' => json_encode($arraydata2),
            'sexo' => json_encode($arraydata3),
            'lugar' => json_encode($arraydata4),
            'categoriasi' => json_encode($arraydata6),

        ];   

        return view('charts.estadistica', $arrayfin);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
