<?php

namespace App\Http\Controllers;

use App\Models\Bomberos;
use App\Models\Rol;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index()
    {
        return view('authentication/index');
    }

    public function register(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:8',
            'id_rol' => 'required',
            'estado' => 'required',
            'id_bombero' => 'required',
        ]);

        $user = User::create([
            'name' => $validateData['name'],
            'email' => $validateData['email'],
            'password' => Hash::make($validateData['password']),
            'estado' => $validateData['estado'],
            'id_rol' => $validateData['id_rol'],
            'id_bombero' => $validateData['id_bombero'],
        ]);

        return response()->json([
            'mensaje' => 'Usuario registrado exitosamente'
        ],200);
    }

    public function rol(Request $request){
        return Role::all();
    }

    public function list(){
        $list = User::join('roles','roles.id','users.id_rol')
                      ->join('bomberos','bomberos.id','users.id_bombero')
                      ->select('users.id','users.name','users.email','roles.nombre as rol'
                      ,Bomberos::raw('CONCAT(bomberos.nombres, " ", bomberos.apellidos) AS nombre')
                    ,'users.estado')->where('users.id', '>', 1);

        return datatables($list)
        ->addColumn('btn','<button class="btn btn-success" onclick="find({{$id}})"><i class="far fa-eye"></i></button>
                           <button class="btn btn-danger" onclick="remove({{$id}})"><i class="fas fa-trash-alt"></i></button>')
        ->rawColumns(['btn'])
        ->make(true);
    }

    public function find(Request $request){
        return User::find($request->id);
    }

    public function update(Request $request){

        $validateData = $request->validate([
            'name' => 'required|string|max:255',
            'password' => 'required|min:8',
            'id_rol' => 'required',
            'estado' => 'required',
            'id_bombero' => 'required',
        ]);
        
        $edit = User::findOrFail($request->id);

        if(!$edit){
            return response()->json([
                'mensaje' => 'El registro no existe'
            ], 401);
        }
        
        $edit->name      = $validateData['name'];
        $edit->password      = $validateData['password'];
        $edit->estado      = $validateData['estado'];
        $edit->id_rol      = $validateData['id_rol'];
        $edit->id_bombero      = $validateData['id_bombero'];

        $edit->save();

        return response()->json([
            'mensaje' => 'modificado.'
        ],200);

    }

    public function delete(Request $request)
    {
        $consulta = User::find($request->id);
        $consulta->delete();

        return response()->json([
            'mensaje' => 'delete correctamente'
        ],200);
    }
}
