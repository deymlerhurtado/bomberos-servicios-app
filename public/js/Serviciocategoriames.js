$(document).ready(function() {
    if ($.fn.dataTable.isDataTable('#tabla')) {} else {
        Listados();

    }

});

function Listados() {
    $("#tabla").dataTable().fnDestroy()


    let _url = `./api/Serviciocategoriames/listcategoriames`;

    $('#tabla').DataTable({
        "lengthMenu": [
            [15, 25, 50, -1],
            [15, 25, 50, "todo"]
        ],
        autoWidth: false,
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: _url,
            data: {}
        },
        "columns": [{
                data: 'categoria'
            },
            {
                data: 'mes'
            },
            {
                data: 'año'
            },
            {
                data: 'total'
            },
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'pdf',
                title: 'Reporte_por_servicios_atendidos_mes'
            },
            {
                extend: 'excelHtml5',
                autoFilter: true,
                sheetName: 'Por_Servicio',
                title: 'Bomberos_Municipales_Departamentales_de_Coatepeque,Quetzaltenango_Reporte_por_servicios'
            }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });
}