<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnidadServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metodo_adquisiciones', function (Blueprint $table) {
            $table->id();
            $table->string('adquisicion');
            $table->timestamps();
        });

        Schema::create('estado_unidades', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion');
        });

        Schema::create('unidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero');
            $table->string('placa');
            $table->string('modelo');
            $table->string('anio');
            $table->unsignedBigInteger('estado_id')->nullable(); //relacion con la tabla categoria
            $table->foreign('estado_id')->references('id')->on('estado_unidades'); //clave foranea   
            $table->unsignedBigInteger('id_metodo_adquisicion');
            $table->foreign('id_metodo_adquisicion')->references('id')->on('metodo_adquisiciones');
            $table->string('foto');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades');
        Schema::dropIfExists('estado_unidades');
        Schema::dropIfExists('metodo_adquisiciones');

    }
}
