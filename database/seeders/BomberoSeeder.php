<?php

namespace Database\Seeders;

use App\Models\Bomberos;
use App\Models\PuestoBomberos;
use Illuminate\Database\Seeder;

class BomberoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bomberoPiloto = PuestoBomberos::create([
            'id' => 1,
            'puesto' => 'Piloto',
            'estado' => 1
        ]);
    }
}
