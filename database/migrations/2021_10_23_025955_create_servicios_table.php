<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->id();
            $table->text('no_reporte')->nullable();
            $table->text('direccion_traslado')->nullable();
            $table->text('traslado')->nullable();
            $table->unsignedBigInteger('id_forma_aviso'); 
            $table->foreign('id_forma_aviso')->references('id')->on('forma_avisos');
            $table->text('telefono')->nullable();
            $table->unsignedBigInteger('id_telefonista_turno');
            $table->foreign('id_telefonista_turno')->references('id')->on('bomberos');
            $table->unsignedBigInteger('id_unidad');
            $table->foreign('id_unidad')->references('id')->on('unidades');
            $table->time('hora_salida');
            $table->time('hora_entrada');
            $table->unsignedBigInteger('id_piloto');
            $table->foreign('id_piloto')->references('id')->on('bomberos');
            $table->date('fecha');
            $table->integer('kilometraje_salida')->nullable();
            $table->integer('kilometraje_entrada')->nullable();
            $table->unsignedBigInteger('id_bombero_reporte');
            $table->foreign('id_bombero_reporte')->references('id')->on('bomberos');
            $table->text('observaciones')->nullable();
            $table->integer('kilometros_recorridos')->nullable();
            $table->time('hora_redaccion');
            $table->string('codigo')->nullable();
            $table->unsignedBigInteger('id_subcategoria_subcategoria');
            $table->foreign('id_subcategoria_subcategoria')->references('id')->on('subcategoria_subcategoria_servicios');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
