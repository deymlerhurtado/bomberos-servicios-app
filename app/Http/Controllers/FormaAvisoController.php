<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forma_aviso;

class FormaAvisoController extends Controller
{
    public function index()
    {
        return view('aviso/FormaAviso');
    }
    public function form()
    {

        return view('avisos/ListadoFormaAviso');
    }
    public function create(Request $request)
    {

        $validateData = $request->validate([
            'forma' => 'required|string|max:255',
            'descripcion' => 'required|string|max:255',
            'estado' => 'required'
        ]);

        $formaAviso = Forma_aviso::create([
            'forma' => $validateData['forma'],
            'descripcion' => $request->descripcion,
            'estado' => $validateData['estado'],
        ]);

        return response()->json([
            'mensaje' => 'Forma de aviso registrada exitosamente.'
        ], 200);
    }

    public function list()
    {

        $list = Forma_aviso::all();
        return datatables($list)
            ->addColumn('btn', '<button class="btn btn-success" onclick="find({{$id}})"><i class="fas fa-edit"></i></button>
                        <button class="btn btn-danger" onclick="remove({{$id}})"><i class="fas fa-trash-alt"></i></button>')
            ->rawColumns(['btn'])
            ->make(true);
    }

    public function find(Request $request)
    {
        return Forma_aviso::find($request->id);
    }

    public function update(Request $request)
    {

        $validateData = $request->validate([
            'id' => 'required',
            'forma' => 'required|string|max:255',
            'descripcion' => 'required|string|max:255',
            'estado' => 'required',
        ]);

        $formaAviso = Forma_aviso::findOrFail($request->id);

        if (!$formaAviso) {
            return response()->json([
                'mensaje' => 'El registro no existe'
            ], 401);
        }

        $formaAviso->forma      = $validateData['forma'];
        $formaAviso->descripcion = $request->descripcion;
        $formaAviso->estado      = $validateData['estado'];
        $formaAviso->save();

        return response()->json([
            'mensaje' => 'Categoria modificada correctamente'
        ], 200);
    }

    public function delete(Request $request)
    {

        try {

            $forma = Forma_aviso::find($request->id);
            $forma->delete();

            return response()->json([
                'mensaje' => 'Categoria eliminada correctamente'
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'El registro está en uso'
            ], 401);
        }
    }

    public function formaAvisos(Request $request)
    {
        return Forma_aviso::where('estado', true)->get();
    }
}
