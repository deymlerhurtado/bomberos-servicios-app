
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#vista_previa').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

// Vista previa imagen seleccionada
$('#foto').change(function () {
    readURL(this);
});


// Muestra el nombre de la foto seleccionada en el cuatro de texto
$('#foto').on('change', function () {
    var fileName = $(this).val().replace('C:\\fakepath\\', " ");

    console.log('selected file: ' + fileName);
    $(this).next('#labelFoto').html(fileName);
});


function readURLUp(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#vista_previaUp').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

