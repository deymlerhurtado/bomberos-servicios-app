<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bomberos;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use datatables;

class BomberosController extends Controller
{
    public function index()
    {
        return view('bomberos/index');
    }
    public function create(Request $request)
    {

        $validateData = $request->validate([
            'nombres' => 'required|string|max:210',
            'apellidos' => 'required|string|max:210',
            'estado' => 'required',
            'dpi' => 'required',
            'telefono' => 'required',
            'fecha_nacimiento' => 'required',
            'no_registro' => 'required',
            'id_puesto' => 'required'
        ]);

        $Bombero = Bomberos::create([
            'nombres' => $validateData['nombres'],
            'apellidos'  => $validateData['apellidos'],
            'id_puesto' => $validateData['id_puesto'],
            'dpi' => $validateData['dpi'],
            'no_registro' => $request->no_registro,
            'email' => $request->email,
            'direccion' => $request->direccion,
            'telefono' => $validateData['telefono'],
            'fecha_nacimiento' => $validateData['fecha_nacimiento'],
            'estado' => $validateData['estado'],
        ]);

        return response()->json([
            'mensaje' => 'Bombero registrado exitosamente.'
        ], 200);
    }
    public function listJson()
    {

        return Bomberos::all();
    }
    public function list()
    {
        $date = (int)Carbon::now()->format('Y');
        $list = Bomberos::join('puesto_bomberos', 'bomberos.id_puesto', '=', 'puesto_bomberos.id')
            ->select(
                'bomberos.id as id',
                Bomberos::raw('CONCAT(bomberos.nombres, " ", bomberos.apellidos) AS nombre'),
                'puesto_bomberos.puesto as puesto',
                'bomberos.dpi as dpi',
                'bomberos.no_registro as no_registro',
                'bomberos.email as email',
                'bomberos.direccion as direccion',
                'bomberos.telefono as telefono',
                'bomberos.fecha_nacimiento as fecha_nacimiento',
                Bomberos::raw('(' . $date . ' - YEAR(bomberos.fecha_nacimiento) ) as edad'),
                'bomberos.estado as estado'
            )->where('bomberos.id', '>', 1)->orderBy('bomberos.id')->get();

        return datatables($list)
            ->addColumn('btn', '<button class="btn btn-success" onclick="find({{$id}})"><i class="far fa-eye"></i></button>
                           <button class="btn btn-danger" onclick="remove({{$id}})"><i class="fas fa-trash-alt"></i></button>')
            ->rawColumns(['btn'])
            ->make(true);
    }

    public function update(Request $request)
    {

        $validateData = $request->validate([
            'id' => 'required',
            'nombres' => 'required|string|max:210',
            'apellidos' => 'required|string|max:210',
            'estado' => 'required',
            'dpi' => 'required',
            'telefono' => 'required',
            'fecha_nacimiento' => 'required',
            'no_registro' => 'required',
            'id_puesto' => 'required'
        ]);

        $edit = Bomberos::findOrFail($request->id);

        if (!$edit) {
            return response()->json([
                'mensaje' => 'El registro no existe'
            ], 401);
        }

        $edit->nombres      = $validateData['nombres'];
        $edit->apellidos      = $validateData['apellidos'];
        $edit->estado      = $validateData['estado'];
        $edit->dpi      = $validateData['dpi'];
        $edit->telefono      = $validateData['telefono'];
        $edit->fecha_nacimiento      = $validateData['fecha_nacimiento'];
        $edit->no_registro      = $validateData['no_registro'];
        $edit->direccion = $request->direccion;
        $edit->id_puesto      = $validateData['id_puesto'];
        $edit->email      = $request->email;

        $edit->save();

        return response()->json([
            'mensaje' => 'modificado.'
        ], 200);
    }

    public function find(Request $request)
    {
        return bomberos::find($request->id);
    }

    public function delete(Request $request)
    {


        try {
            $consulta = Bomberos::find($request->id);
            $consulta->delete();

            return response()->json([
                'mensaje' => 'delete correctamente'
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'El registro está en uso'
            ], 401);
        }
    }


    public function bomberos(Request $request)
    {
        return response()->json(Bomberos::where('estado', true)->where('id', '>', 1)->get());
    }
    public function bomberosPiloto(Request $request)
    {
        return Bomberos::where('estado', true)->where('id_puesto', 1)->where('id', '>', 1)->get();
    }

    public function serviciosJson(Request $request)
    {
        $consulta = DB::select('SELECT B.nombres as nombre, B.apellidos as apellido, B.dpi as dpi, B.telefono as telefono, B.no_registro as no_registro, 
        B.direccion as direccion, P.puesto as puesto
        FROM servicio_asistentes SA
        Inner Join bomberos B ON B.id= SA.id_asistente
        INNER JOIN puesto_bomberos P ON P.id = B.id_puesto
        where SA.id_servicio =' . $request->id);

        return $consulta;
    }
}
