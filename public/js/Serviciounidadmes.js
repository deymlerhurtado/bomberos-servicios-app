$(document).ready(function() {
    if ($.fn.dataTable.isDataTable('#tabla')) {} else {
        Listados();

    }

});

function Listados() {
    $("#tabla").dataTable().fnDestroy()


    let _url = `./api/Serviciounidadmes/listunidadmes`;

    $('#tabla').DataTable({
        "lengthMenu": [
            [15, 25, 50, -1],
            [15, 25, 50, "todo"]
        ],
        autoWidth: false,
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: _url,
            data: {}
        },
        "columns": [{
                data: 'Mes'
            },
            {
                data: 'mes'
            },
            {
                data: 'año'
            },
            {
                data: 'numero'
            },
            {
                data: 'total'
            },
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'pdf',
                title: 'Reporte_servicios_unidad_mes'
            },
            {
                extend: 'excelHtml5',
                autoFilter: true,
                sheetName: 'Por_Unidad',
                title: 'Bomberos_Municipales_Departamentales_de_Coatepeque,Quetzaltenango_Reporte_Unidad'
            }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });
}