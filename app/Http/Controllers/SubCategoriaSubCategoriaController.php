<?php

namespace App\Http\Controllers;

use App\Models\SubcategoriaSubcategoriaServicio;
use Illuminate\Http\Request;
use DB;

class SubCategoriaSubCategoriaController extends Controller
{
    public function index()
    {

        return view('subcategoriasubcategoria/SubCategoriaSubCategoriaServicio');
    }
    public function create(Request $request)
    {

        $validateData = $request->validate([
            'subcategoria' => 'required|string|max:255',
            'id_subcategoria' => 'required',
            'estado' => 'required'
        ]);

        $SubcategoriaServicio = SubcategoriaSubcategoriaServicio::create([
            'subcategoria' => $validateData['subcategoria'],
            'descripcion' => $request->descripcion,
            'id_subcategoria' => $validateData['id_subcategoria'],
            'estado' => $validateData['estado'],
        ]);

        return response()->json([
            'mensaje' => 'SubCategoria registrada exitosamente.'
        ], 200);
    }
    public function listJson()
    {

        return SubcategoriaSubcategoriaServicio::all();
    }
    public function list()
    {

        $list = DB::select('select sss.id, sss.subcategoria as nombre, sss.estado, sss.descripcion, ss.subcategoria, cs.categoria from subcategoria_subcategoria_servicios sss
        inner join subcategoria_servicios ss on ss.id = sss.id_subcategoria
        inner join categoria_servicios cs on cs.id = ss.id_categoria');

            return datatables($list)
            ->addColumn('btn', '<button class="btn btn-success" onclick="find({{$id}})"><i class="fas fa-edit"></i></button>
                           <button class="btn btn-danger" onclick="remove({{$id}})"><i class="fas fa-trash-alt"></i></button>')
            ->rawColumns(['btn'])
            ->make(true);
    }

    public function find(Request $request)
    {
        $find = SubcategoriaSubcategoriaServicio::join('subcategoria_servicios', 'subcategoria_servicios.id', 'subcategoria_subcategoria_servicios.id_subcategoria')
            ->join('categoria_servicios', 'categoria_servicios.id', 'subcategoria_servicios.id_categoria')
            ->select('subcategoria_subcategoria_servicios.id', 'subcategoria_subcategoria_servicios.subcategoria as nombre', 'subcategoria_subcategoria_servicios.estado', 'subcategoria_subcategoria_servicios.descripcion', 'subcategoria_servicios.id as id_subcategoria', 'categoria_servicios.id as id_categoria')
            ->where('subcategoria_subcategoria_servicios.id', $request->id)->first();

        return $find;
    }

    public function update(Request $request)
    {

        $validateData = $request->validate([
            'id' => 'required',
            'subcategoria' => 'required|string|max:255',
            'id_subcategoria' => 'required',
            'estado' => 'required'
        ]);

        $SubcategoriaServicio = SubcategoriaSubcategoriaServicio::findOrFail($request->id);

        if (!$SubcategoriaServicio) {
            return response()->json([
                'mensaje' => 'El registro no existe'
            ], 401);
        }

        $SubcategoriaServicio->subcategoria      = $validateData['subcategoria'];
        $SubcategoriaServicio->descripcion = $request->descripcion;
        $SubcategoriaServicio->id_subcategoria      = $validateData['id_subcategoria'];
        $SubcategoriaServicio->estado      = $validateData['estado'];
        $SubcategoriaServicio->save();

        return response()->json([
            'mensaje' => 'SubCategoria modificada exitosamente.'
        ], 200);
    }

    public function subcategorias(Request $request)
    {
        return SubcategoriaSubcategoriaServicio::where('estado', true)->where('id_subcategoria', $request->id)->get();
    }
    public function delete(Request $request)
    {

        try {
            $subcategoria = SubcategoriaSubcategoriaServicio::find($request->id);
            $subcategoria->delete();

            return response()->json([
                'mensaje' => 'Categoria eliminada correctamente'
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'El registro está en uso'
            ], 401);
        }
    }
}
