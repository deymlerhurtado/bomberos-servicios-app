<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *s
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if($request->user()->id_rol == 1){

            return redirect('/');
            
        }else if ($request->user()->id_rol == 2){   

            return redirect()->route('index.servicios');

        }
    }
}
