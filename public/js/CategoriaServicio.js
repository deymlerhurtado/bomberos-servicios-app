$(document).ready(function() {
    if ($.fn.dataTable.isDataTable('#tabla')) {} else {
        Listado();

    }


    $("#formUpdate").on('submit', function(evt) {
        evt.preventDefault();

        if ($("#formUpdate")[0].checkValidity() == true) {

            let id = $('#id').val();
            console.log(id);
            if (id != 0 || id > 0) {
                update();
            } else {
                CrearCategoria();
            }
        } else {
            console.log("Form incorrecto")

        }
    });

});

function Listado() {
    $("#tabla").dataTable().fnDestroy()


    let _url = `./api/categoriaServicio/list`;

    $('#tabla').DataTable({
        autoWidth: false,
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: _url,
            data: {}
        },
        "columns": [{
                data: 'identificador'
            },
        {
                data: 'categoria'
            },
            {
                data: 'descripcion'
            },
            {data:'estado',
            render: function(data, type, row) {
  
                if(row.estado == 1) {
                    return "Activo";
                }else{
                    return "Inactivo";  
                }
                }
            },
            {
                data: 'btn'
            },
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'pdf',
                title: 'Reporte Categoria Servicio'
            }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

}


function modalCreate() {
    $("#formUpdate input:hidden").val('');

    $('#btneditar').hide();
    $('#title').text('Crear categoría');
    $('#guardar').show();
    $('#modal').modal('show');
}

function CrearCategoria() {



    let categoria = $('#categoria').val();
    let identificador = $('#identificador').val();
    let descripcion = $('#descripcion').val();
    let estado = $('#estado').val();

    console.log(categoria, descripcion, estado);

    let _url = `./api/categoriaServicio/create`;
    let _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: _url,
        type: 'post',
        data: {
            categoria: categoria,
            identificador: identificador,
            descripcion: descripcion,
            estado: estado,
            _token: _token
        },
        dataType: 'JSON',
        success: function(response) {
            if (response) {
                console.log(response);
                $('#formUpdate').trigger("reset");
                $('#tabla').DataTable().ajax.reload();
                $('#modal').modal('hide');
            }
        },
        error: function(errormessage) {
            console.log(errormessage.responseText);
        }
    });

}


function find(id) {
    console.log(id);
    let _url = `./api/categoriaServicio/find?id=${id}`;

    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function(response) {
            let html = '';
            if (response) {
                $('#id').val(response.id);
                $('#categoria').val(response.categoria);
                $('#identificador').val(response.identificador);
                $('#descripcion').val(response.descripcion);
                $('#estado').val(response.estado);
                $('#title').text('Editar categoría');
                $('#guardar').hide();
                $('#btneditar').show();
                $("#modal").modal('show');

            }
        },
        error: function(errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function update() {
    let id = $('#id').val();
    let categoria = $('#categoria').val();
    let identificador = $('#identificador').val();
    let descripcion = $('#descripcion').val();
    let estado = $('#estado').val();

    console.log(id, categoria, descripcion, estado);

    let _url = `./api/categoriaServicio/update`;
    let _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: _url,
        type: 'put',
        data: {
            id: id,
            categoria: categoria,
            identificador: identificador,
            descripcion: descripcion,
            estado: estado,
            _token: _token
        },
        dataType: 'JSON',
        success: function(response) {
            if (response) {
                console.log(response);
                $('#formUpdate').trigger("reset");
                $("#formUpdate input:hidden").val(' ');
                $('#tabla').DataTable().ajax.reload();

                $("#modal").modal('hide');

            }
        },
        error: function(errormessage) {
            console.log(errormessage.responseText);
        }
    });
}

function remove(id){
    Swal.fire({
        title: '¿esta seguro?',
        text: "¿seguro que quieres eliminar?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'si, seguro!'
      }).then((result) => {
        if (result.isConfirmed) {

            let _url = `./api/categoriaServicio/delete?id=${id}`;
            let _token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: _url,
            type: 'delete',
            data: {
            },
            dataType: 'JSON',
            success: function (response) {
                if (response) {
                    console.log(response);
                    $('#tabla').DataTable().ajax.reload();
                    toastr.success("eliminado");

                }
            },
            error: function (errormessage) {
                toastr.error("El registro está en uso");

                console.log(errormessage.responseText);
            }
        });
      
    }
  })
}
