$(document).ready(function () {
    selectPuestos();
    if ( $.fn.dataTable.isDataTable( '#tabla' ) ) {
    }else{
        listBomberos();
    }

    $("#form").on('submit', function (evt) {
        evt.preventDefault();

        if ($("#form")[0].checkValidity() == true) {

            let id = $('#id').val();
            console.log(id);
            if (id != 0 || id > 0) {
                update();
            } else {
                create();
            }
        } else {
            console.log("Form incorrecto")

        }
    });



});
function modalCreate() {
    document.getElementsByClassName('needs-validation')[0].classList.remove("was-validated");
    $("#form input:hidden").val('');
    $('#form').trigger('reset');
    $('#edit').hide();
    $('#titulo').text('Registrar nuevo colaborador');
    $('#add').show();
    $('#modal').modal('show');
}



function find(id){

    let _url = `./api/bomberos/find?id=${id}`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {

            $('#id').val(response.id);
            $('#nombres').val(response.nombres);
            $('#apellidos').val(response.apellidos);
            $('#dpi').val(response.dpi);
            $('#direccion').val(response.direccion);
            $('#fecha_nacimiento').val(response.fecha_nacimiento);
            $('#telefono').val(response.telefono);
            $('#email').val(response.email);
            $('#estado').val(response.estado);
            $('#id_puesto').val(response.id_puesto);
            $('#no_registro').val(response.no_registro);
            $("#titulo").text('Editar colaborador');
            $('#edit').show();
            $('#add').hide();
            $('#form').attr('action', '');
            $("#modal").modal('show');
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });

}
function selectPuestos() {
    let _url = `./api/puestoBomberos/listJson`;
    $.ajax({
        url: _url,
        type: 'get',
        data: {},
        dataType: 'JSON',
        success: function (response) {
            let respuesta = response;
            let html = '`<option value="" selected hidden>Elegir..</option>`';
            respuesta.forEach(respuestas => {
                html += `<option value="${respuestas.id}">${respuestas.puesto}</option>`
            });
            $('#id_puesto').html(html);

        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });
}
function listBomberos() {
    $('#tabla').DataTable( {
        autoWidth: false,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "todo"]],
        "order": [[ 0, "desc" ]],
        "ajax": "./api/bomberos/list",
        "columns":[
            {data:'nombre'},
            {data:'dpi'},
            {data:'puesto'},
            {data:'email'},
            {data:'edad'},
            {data:'telefono'},
            {data:'no_registro'},
            {data:'direccion'},
            {data:'estado'},
            {data:'btn'},
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'print',
                title: 'Reporte colaboradores'
               }
        ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                buttons: {
                    print: 'imprimir'
                }
                
            }
    } );

}
function create(){
    let nombres = $('#nombres').val();
    let apellidos = $('#apellidos').val();
    let dpi = $('#dpi').val();
    let telefono = $('#telefono').val();
    let direccion = $('#direccion').val();
    let email = $('#email').val();
    let fecha_nacimiento = $('#fecha_nacimiento').val();
    let no_registro = $('#no_registro').val();
    var id_puesto =$('#id_puesto').val();
    var estado =$('#estado').val();
        let _url= `./api/bomberos/create`;
        let _token   = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: _url,
        type: 'post',
        data : {
            nombres: nombres,
            apellidos: apellidos,
            dpi: dpi,
            telefono: telefono,
            direccion:direccion,
            email:email,
            fecha_nacimiento:fecha_nacimiento,
            no_registro: no_registro,
            id_puesto: id_puesto,
            estado: estado,
            _token: _token},
        dataType: 'JSON',
        success: function (response) {
            $('#tabla').DataTable().ajax.reload();
            if(response){
            console.log("agregado");
            $("#modal").modal('hide');
            toastr.success("Creado con exito");
            }
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });

}
function update(){
    let _url= `./api/bomberos/update`;
    let _token   = $('meta[name="csrf-token"]').attr('content');
    let nombres = $('#nombres').val();
    let apellidos = $('#apellidos').val();
    let dpi = $('#dpi').val();
    let telefono = $('#telefono').val();
    let direccion = $('#direccion').val();
    let email = $('#email').val();
    let fecha_nacimiento = $('#fecha_nacimiento').val();
    let no_registro = $('#no_registro').val();
    var id_puesto =$('#id_puesto').val();
    var estado =$('#estado').val();
    var id= $('#id').val();

    $.ajax({
        url: _url,
        type: 'post',
        data : {
            id: id,
            nombres: nombres,
            apellidos: apellidos,
            dpi: dpi,
            telefono: telefono,
            direccion:direccion,
            email:email,
            fecha_nacimiento:fecha_nacimiento,
            no_registro: no_registro,
            id_puesto: id_puesto,
            estado: estado,
            _token: _token},
        dataType: 'JSON',
        success: function (response) {
            $('#tabla').DataTable().ajax.reload();
            document.getElementsByClassName('needs-validation')[0].classList.remove("was-validated");
            $("#modal").modal('hide');
            toastr.success("actualizado con exito");
        },
        error: function (errormessage) {
            console.log(errormessage.responseText);
        }
    });

}
function remove(id){
    Swal.fire({
        title: '¿esta seguro?',
        text: "¿seguro que quieres eliminar?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'si, seguro!'
      }).then((result) => {
        if (result.isConfirmed) {

        let _url = `./api/bomberos/delete?id=${id}`;
        let _token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: _url,
            type: 'delete',
            data: {
            },
            dataType: 'JSON',
            success: function (response) {
                if (response) {
                    console.log(response);
                    $('#tabla').DataTable().ajax.reload();
                    toastr.success("eliminado");

                }
            },
            error: function (errormessage) {
                toastr.error("El registro está en uso");

                console.log(errormessage.responseText);
            }
        });
      
    }
  })
}

