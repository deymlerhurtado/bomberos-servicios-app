<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubcategoriaServicio extends Model
{
    use HasFactory;
    protected $fillable = [
        'subcategoria',
        'descripcion',
        'id_categoria',
        'estado'
    ];
    //para la relacion entre las tablas
    public function CategoriaServicio()
    {
        return $this->belongsTo('App\CategoriaServicio','id_categoria');
    }
    /*protected $primaryKey = "id";
    public function CategoriaServicio(){
        return $this->belongsTo('App\Models\CategoriaServicio', 'id', 'id_categoria'); 
    }*/
}
